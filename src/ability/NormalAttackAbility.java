package ability;

import soundplayer.SoundPlayer;
import characters.GameCharacter;
import characters.playable.PlayableCharacter;

public class NormalAttackAbility implements Ability {
	private int attackPower;
	private String description;
	private String attacker;

	public NormalAttackAbility(int attackPower, String attacker) {
		this.attackPower = attackPower;
		this.description = "+ " + attackPower
				+ " damage";
		this.attacker = attacker;
	}

	@Override
	public void execute(GameCharacter c) {
		
		
		//Human attack sounds
		if (attacker.equals("Assault")){
			SoundPlayer.playFile("./SkynetSounds/shot3.mp3");
			try {
			    Thread.sleep(85);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			SoundPlayer.playFile("./SkynetSounds/shot3.mp3");
			try {
			    Thread.sleep(85);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			SoundPlayer.playFile("./SkynetSounds/shot3.mp3");
		}
		if (attacker.equals("Captain")){
			SoundPlayer.playFile("./SkynetSounds/shot5.mp3");
			try {
			    Thread.sleep(95);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			SoundPlayer.playFile("./SkynetSounds/shot5.mp3");
		}
		if (attacker.equals("Gunman")){
			SoundPlayer.playFile("./SkynetSounds/explosion2.mp3");
			try {
			    Thread.sleep(110);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			SoundPlayer.playFile("./SkynetSounds/explosion2.mp3");
			try {
			    Thread.sleep(110);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			SoundPlayer.playFile("./SkynetSounds/explosion2.mp3");
		}
		if (attacker.equals("MachineGunner")){
			SoundPlayer.playFile("./SkynetSounds/shot10RapidFire.mp3");
		}
		if (attacker.equals("Rifleman")){
			SoundPlayer.playFile("./SkynetSounds/shot9.mp3");
		}
		
		//Enemy attack sounds
		if (attacker.equals("Zombie")){
			SoundPlayer.playFile("./SkynetSounds/roar5.mp3");
		}
		if (attacker.equals("Strong Zombie")){
			SoundPlayer.playFile("./SkynetSounds/alien2.mp3");
		}
		if (attacker.equals("Item Zombie")){
			SoundPlayer.playFile("./SkynetSounds/roar5.mp3");
		}
		if (attacker.equals("King Zombie")){
			SoundPlayer.playFile("./SkynetSounds/mechanical4.mp3");
		}
		
		//Checks creature being attacked for appropriate sound
		if (c.getName().equals("Door")){
			SoundPlayer.playFile("./SkynetSounds/doorOpening1.mp3");
		}
		else if (!(c instanceof PlayableCharacter)){
			//SoundPlayer.playFile("./SkynetSounds/shot10RapidFire.mp3");
			SoundPlayer.playFile("./SkynetSounds/moan1.mp3");
		}
		else {
			//SoundPlayer.playFile("./SkynetSounds/alien2.mp3");
			SoundPlayer.playFile("./SkynetSounds/humanDeath3.mp3");
		}
			
		
		c.changeHealth(-1 * attackPower);
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getName() {
		return "Attack";
	}

	@Override
	public void increaseAttackAbility(int amount) {
		attackPower += amount;

	}

}
