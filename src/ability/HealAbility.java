package ability;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class HealAbility implements Ability {
	private int healPower;
	private String description;

	public HealAbility(int heal) {
		this.healPower = heal;
		this.description = "Can heal a character by " + heal + " HP";
	}

	@Override
	public void execute(GameCharacter c) {
		SoundPlayer.playFile("./SkynetSounds/BLEEP3.mp3");
		c.changeHealth(healPower);
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getName() {
		return "Heal";
	}

	@Override
	public void increaseAttackAbility(int amount) {
		healPower += amount;

	}

}
