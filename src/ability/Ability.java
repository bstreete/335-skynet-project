package ability;

import java.io.Serializable;

import characters.GameCharacter;

public interface Ability extends Serializable{
	public void execute(GameCharacter c);
	public String getDescription();
	public String getName();
	public void increaseAttackAbility(int amount);
}
