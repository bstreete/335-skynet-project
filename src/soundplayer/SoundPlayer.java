package soundplayer;

import soundplayer.AudioFilePlayer;

/**
 * SongPlayer has two static methods that allow an audio file to be played
 * through the output device. The first one
 * 
 * This small class was added as a bridge to a more complicated type in an
 * effort to make it easier to use. It also exists so 335 can postpone coverage
 * of concurrency with Threads. It has an interface similar to import
 * javax.swing.Timer in that you create a listener first than call the one
 * method playFile with that listener as the first argument.
 */

public class SoundPlayer {
  /**
   * Play the audio file stored in audioFileName
   * 
   * @param audioFileName
   *          The name of the file to be written to your output device.
   */
	private volatile AudioFilePlayer player;
	private boolean replay;
	
	public SoundPlayer(String audioFileName) {
		player = new AudioFilePlayer(audioFileName);
		// AudioFilePlayer extends Thread. When start is called,
		// the overridden run method in AudioFilePlayer executes.
		// If the song is not played in a separate thread, your GUI stops
		// working
		player.start();
		replay=false;
		
	}
	
	public SoundPlayer(String audioFileName, boolean replay){
		this.replay=replay;
		player = new AudioFilePlayer(audioFileName);
		ObjectWaitingForSongToEnd waiter = new ObjectWaitingForSongToEnd();
		player.addEndOfSongListener(waiter);
		player.start();		
	}
	
	public class ObjectWaitingForSongToEnd implements EndOfSongListener{

		@Override
		public void songFinishedPlaying(
				EndOfSongEvent eventWithFileNameAndDateFinished) {
			player.stopPlaying();
			if(replay){
			System.out.println("Reached end of song, playing again.");
			player = new AudioFilePlayer("./SkynetSongs/MainMenuMusic.mp3");
			ObjectWaitingForSongToEnd waiter = new ObjectWaitingForSongToEnd();
			player.addEndOfSongListener(waiter);
			player.start();
			}
		}
	}
	
	public static void playFile(String audioFileName) {
		AudioFilePlayer player = new AudioFilePlayer(audioFileName);
		// AudioFilePlayer extends Thread. When start is called,
		// the overridden run method in AudioFilePlayer executes.
		// If the song is not played in a separate thread, your GUI stops
		// working
		player.start();
	}

	public void stopPlaying() {
		player.stopPlaying();
		this.replay=false;
	}
}