package items;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class Painkiller extends GameItem {
	private static final long serialVersionUID = 5244434342564500655L;

	public Painkiller(int row, int col) {
		super(row, col, "Painkiller", "painkiller.png",
				"+100HP.");

	}

	public String useOn(GameCharacter c) {
		c.changeHealth(500);
		SoundPlayer.playFile("./SkynetSounds/BLEEP3.mp3");
		return "Added 500 to " + c.getName() + "'s health.";
	}
}
