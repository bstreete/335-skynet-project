package items;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class Stimulant extends GameItem{
	private static final long serialVersionUID = 5721703957044506635L;

	public Stimulant(int row, int col) {
		super(
				row,
				col,
				"Stimulant",
				"stimulant.png",
				"+500HP +3RNG + 3MBL");
	}

	public String useOn(GameCharacter c) {
		c.changeHealth(500);
		c.changeMobility(3);
		c.changeRange(3);
		SoundPlayer.playFile("./SkynetSounds/doorOpening4.mp3");
		return "Added 500 to life, 3 to range, and 3 to mobility.";
	}
}
