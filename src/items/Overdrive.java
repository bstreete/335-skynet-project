package items;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class Overdrive extends GameItem{
	private static final long serialVersionUID = -2137699971921492561L;
	public Overdrive(int row, int col) {
		super(row, col, "Overdrive", "overdrive.png",
				"+3MBL");
	}
	
	public String useOn(GameCharacter c) {
		c.changeMobility(3);
		SoundPlayer.playFile("./SkynetSounds/mechanical1.mp3");
		return "Increased " + c.getName() + "'s mobility by 3";
	}
}
