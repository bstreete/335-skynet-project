package items;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class Armor extends GameItem {
	private static final long serialVersionUID = 3844115427871545334L;

	public Armor(int row, int col) {
		super(
				row,
				col,
				"Armor",
				"armor.png",
				"+100HP");

	}

	public String useOn(GameCharacter c) {
		c.changeHealth(600);
		SoundPlayer.playFile("./SkynetSounds/put.mp3");
		return "Added 600 to " + c.getName() + "'s life.";

	}

}
