package items;

import characters.GameCharacter;
import characters.GameObject;

public abstract class GameItem extends GameObject {
	private static final long serialVersionUID = -6158014394236099103L;

	public GameItem(int r, int c, String n, String i, String d) {
		super(n, r, c, i, d);
	}

	public abstract String useOn(GameCharacter c);

}
