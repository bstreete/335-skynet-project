package items;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

public class Optics extends GameItem{
	private static final long serialVersionUID = 5494508530778030450L;

	public Optics(int row, int col) {
		super(
				row,
				col,
				"Optics",
				"improvedOptics.png",
				"+1000ATK");
	}

	public String useOn(GameCharacter c) {
		c.getAbility().increaseAttackAbility(1000);
		SoundPlayer.playFile("./SkynetSounds/reload1.mp3");
		return "Increased " + c.getName() + "'s attack power by 1000.";
	}

}
