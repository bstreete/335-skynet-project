package maps;

import java.util.List;

import characters.GameCharacter;

public class EliminationCondition implements WinningCondition {
	private static final long serialVersionUID = 7602938529847817743L;

	List<GameCharacter> goodGuys;
	List<GameCharacter> badGuys;

	public EliminationCondition(List<GameCharacter> goodList,
			List<GameCharacter> badList) {
		goodGuys = goodList;
		badGuys = badList;
	}

	@Override
	public boolean checkWin() { // you win when there are no more bad guys left
		return badGuys.isEmpty();
	}

	@Override
	public boolean checkLose() { // you lose when all your units are dead
		return goodGuys.isEmpty();
	}

}
