package maps;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

/**disguises as normal tile until stepped on
 * heals first player to step on it
 * 
 *
 */
public class HealTile extends Tile{
	private boolean steppedOn;
	public HealTile() {
		super(true, false, "normalTile.png");
		// TODO Auto-generated constructor stub
		steppedOn=false;
	}

	@Override
	public void stepOnEffect(GameCharacter character) {
		if(!steppedOn){
		character.setHealth(character.getHealth());
		//TODO: can add sound here
		
		SoundPlayer.playFile("./SkynetSounds/crystal1.mp3");
		
		this.changeImage("healTile.png");
		steppedOn=true;
		}
	}

}
