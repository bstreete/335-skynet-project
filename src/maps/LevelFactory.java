package maps;

import items.Armor;
import items.GameItem;
import items.Optics;
import items.Overdrive;
import items.Painkiller;
import items.Stimulant;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import characters.GameCharacter;
import characters.GameObject;
import characters.playable.Assault;
import characters.playable.Captain;
import characters.playable.Gunman;
import characters.playable.MachineGunner;
import characters.playable.Medic;
import characters.playable.PlaceHolder;
import characters.playable.PlayableCharacter;
import characters.playable.Rifleman;
import characters.props.Door;
import characters.unplayable.ItemZombie;
import characters.unplayable.KingZombie;
import characters.unplayable.StrongZombie;
import characters.unplayable.Zombie;
import characters.props.Door;

//import characters.unplayable.Door;

public class LevelFactory {
	// tile code
	private static final char NORMAL = 'N';
	private static final char WALL = 'W';
	private static final char EMPTY = 'E';
	private static final char INNERCORNER = 'C';
	private static final char OUTERCORNER = 'O';

	private static final char SURVIVALTILE = 'S';
	private static final char HEAL = 'H';
	private static final char TRAP = 'T';

	// game character code
	private static final String ZOMBIE = "ZOMBIE";
	private static final String ITEMZOMBIE = "ITEMZOMBIE";
	private static final String STRONGZOMBIE = "STRONGZOMBIE";
	private static final String KINGZOMBIE = "KINGZOMBIE";
	private static final String RIFLEMAN = "RIFLEMAN";
	private static final String MEDIC = "MEDIC";
	private static final String STOP = "STOP";
	private static final String CAPTAIN = "CAPTAIN";
	private static final String ASSAULT = "ASSAULT";
	private static final String MACHINEGUNNER = "MACHINEGUNNER";
	private static final String GUNMAN = "GUNMAN";
	// game item code
	private static final String ARMOR = "ARMOR";
	private static final String BOOTS = "BOOTS";
	private static final String PAINKILLER = "PAINKILLER";
	private static final String CAFFEINE = "CAFFEINE";
	private static final String TESTOSTERONE = "TESTOSTERONE";
	// other
	private static final String DOOR = "DOOR";
	private static final String PLACEHOLDER = "PLACEHOLDER";
	// winning condition code
	private static final String ELIMINATION = "ELIMINATION";
	private static final String ASSASSINATION = "ASSASSINATION";
	private static final String SURVIVAL = "SURVIVAL";

	private Tile[][] map;

	private List<GameCharacter> badList;
	private List<GameCharacter> goodList;
	private List<GameCharacter> placeholders;
	private List<GameItem> itemList;
	private String description;
	WinningCondition win;
	SurvivalTile survival;
	List<SurvivalTile> survivalTile;

	public Tile[][] makeLevel(String descriptionFile, String mapFile) {
		// System.out.println("Making Level");

		goodList = new ArrayList<GameCharacter>();
		badList = new ArrayList<GameCharacter>();
		itemList = new ArrayList<GameItem>();
		placeholders=new ArrayList<GameCharacter>();
		survivalTile = new ArrayList<SurvivalTile>();

		try {
			// System.out.println("Getting Description");
			// get level description
			Scanner reader = new Scanner(new File(descriptionFile));
			description = "<html>";
			while (reader.hasNextLine()) {
				description += reader.nextLine() + "<br>";
			}
			description += "</html>";
			reader.close();
			// Then make map
			Scanner read = new Scanner(new File(mapFile));
			int rows = Integer.parseInt((read.nextLine()));
			int cols = Integer.parseInt((read.nextLine()));
			// System.out.println(rows + " rows");
			// System.out.println(cols + " cols");
			map = new Tile[rows][cols];
			// System.out.println("Making map");
			for (int i = 0; i < rows; i++) {
				// System.out.println("Creating row " + i);
				String line = read.nextLine();
				for (int j = 0; j < cols; j++) {
					Tile tile;
					switch (line.charAt(j)) {
					case NORMAL:
						tile = new NormalTile();
						break;
					case WALL:
						tile = new WallTile();
						break;
					case EMPTY:
						tile = new EmptyTile();
						break;
					case INNERCORNER:
						tile = new CornerTile("insideCorner.png");
						break;
					case OUTERCORNER:
						tile = new CornerTile("outsideCorner.png");
						break;

					case SURVIVALTILE:
						survival = new SurvivalTile();
						survivalTile.add(survival);
						tile = survival;
						break;
					case HEAL:
						tile = new HealTile();
						break;
					case TRAP:
						tile = new TrapTile();
						break;
					default:
						tile = new EmptyTile();
						break;
					}
					map[i][j] = tile;
				}
			}

			// Rotate tiles correctly for graphics
			map = calculateTileRotate(map);

			// then put starting characters and equipment in
			// System.out.println("Getting characters");

			String line = read.nextLine().trim();
			while (!line.equals(STOP)) {
				int row = Integer.parseInt(line);// first int is row
				int col = Integer.parseInt(read.nextLine().trim());// second int
																	// is col
				String characterType = read.nextLine().trim();
				GameCharacter character = null;
				GameItem item;
				switch (characterType) {
				case ZOMBIE:
					// System.out.println("Adding zombie to " + row + " " +
					// col);
					character = new Zombie(row, col);
					badList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case ITEMZOMBIE:
					// System.out.println("Adding itemzombie to " + row + " "
					// + col);
					character = new ItemZombie(row, col);
					badList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case STRONGZOMBIE:
					// System.out.println("Adding strongzombie to " + row + " "
					// + col);
					character = new StrongZombie(row, col);
					badList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case KINGZOMBIE:
					// System.out.println("Adding kingzombie to " + row + " "
					// + col);
					character = new KingZombie(row, col);
					badList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case RIFLEMAN:
					// System.out.println("Adding rifleman to " + row + " " +
					// col);
					character = new Rifleman(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case MEDIC:
					// System.out.println("Adding medic to " + row + " " + col);
					character = new Medic(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case CAPTAIN:
					// System.out.println("Adding captain to " + row + " " +
					// col);
					character = new Captain(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case ASSAULT:
					// System.out.println("Adding assault to " + row + " " +
					// col);
					character = new Assault(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case MACHINEGUNNER:
					// System.out.println("Adding machinegunner to " + row + " "
					// + col);
					character = new MachineGunner(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case GUNMAN:
					// System.out.println("Adding gunman to " + row + " " +
					// col);
					character = new Gunman(row, col);
					goodList.add(character);
					map[row][col].setOccupant(character);
					map[row][col].setAccessible(false);
					break;
				case BOOTS:
					// System.out.println("Adding boots to " + row + " " + col);
					item = new Overdrive(row, col);
					map[row][col].setOccupant(item);
					itemList.add(item);
					break;
				case ARMOR:
					// System.out.println("Adding armor to " + row + " " + col);
					item = new Armor(row, col);
					map[row][col].setOccupant(item);
					itemList.add(item);
					break;
				case CAFFEINE:
					// System.out.println("Adding caffeine to " + row + " " +
					// col);
					item = new Optics(row, col);
					map[row][col].setOccupant(item);
					itemList.add(item);
					break;
				case TESTOSTERONE:
					// System.out.println("Adding testosterone to " + row + " "
					// + col);
					item = new Stimulant(row, col);
					map[row][col].setOccupant(item);
					itemList.add(item);
					break;
				case PAINKILLER:
					// System.out.println("Adding painkiller to " + row + " "
					// + col);
					item = new Painkiller(row, col);
					map[row][col].setOccupant(item);
					itemList.add(item);
					break;
				case DOOR:
					map[row][col].setOccupant(new Door(row, col, true));
					map[row][col].setAccessible(false);
					break;
				case PLACEHOLDER:
					PlaceHolder p = new PlaceHolder(row, col);
					map[row][col].setOccupant(p);
					placeholders.add(p);
					break;
				default:
					// do nothing
				}

				line = read.nextLine().trim();
			}
			// create winning condition

			// System.out.print("Setting win condition to ");
			switch (read.nextLine()) {
			case ELIMINATION:
				// System.out.println("Elimination");
				win = new EliminationCondition(goodList, badList);
				break;
			case ASSASSINATION:
				// expect 2 more lines of coordinates
				// System.out.println("Assasination");
				int row1 = read.nextInt();
				int col1 = read.nextInt();
				GameCharacter badKing = new KingZombie(row1, col1);
				map[row1][col1].setOccupant(badKing);
				map[row1][col1].setAccessible(false);
				badList.add(badKing);
				win = new AssassinationCondition(goodList, badKing);
				break;
			case SURVIVAL:
				System.out.println("Survival");
				win = new SurvivalCondition(goodList, survivalTile);
				break;
			default:
				win = new EliminationCondition(goodList, badList); // default
																	// winning
																	// condition
																	// is
																	// elimination
			}
			read.close();
			return map;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

	}

	/*
	 * Calculates how much to rotate floor tiles so they display correctly in
	 * the GUI. Corner pieces start with the corner in the top right corner.
	 * Walls start with the floor along the top edge.
	 */
	private Tile[][] calculateTileRotate(Tile[][] map) {

		// Changes the corners and walls for all interior squares
		for (int x = 1; x < map.length - 1; x++)
			for (int y = 1; y < map[x].length - 1; y++) {
				Tile current = map[x][y];
				String filename = current.getImageFile();

				switch (filename) {
				case "normalTile.png":
				case "emptyTile.jpg":
					break;
				case "wallTile.png":
					if (map[x + 1][y] instanceof NormalTile)
						current.changeImage("bWall.png");
					else if (map[x][y + 1] instanceof NormalTile)
						current.changeImage("rWall.png");
					else if (map[x][y - 1] instanceof NormalTile)
						current.changeImage("lWall.png");
					else
						current.changeImage("tWall.png");
					break;

				case "insideCorner.png":
					if (map[x + 1][y + 1] instanceof NormalTile)
						current.changeImage("brInCorner.png");
					else if (map[x - 1][y + 1] instanceof NormalTile)
						current.changeImage("trInCorner.png");
					else if (map[x + 1][y - 1] instanceof NormalTile)
						current.changeImage("blInCorner.png");
					else
						current.changeImage("tlInCorner.png");
					break;

				case "outsideCorner.png":
					if (map[x + 1][y + 1] instanceof EmptyTile)
						current.changeImage("tlOutCorner.png");
					else if (map[x - 1][y + 1] instanceof EmptyTile)
						current.changeImage("blOutCorner.png");
					else if (map[x + 1][y - 1] instanceof EmptyTile)
						current.changeImage("trOutCorner.png");
					else
						current.changeImage("brOutCorner.png");
					break;
				}

				map[x][y] = current;
			}

		// Corners
		if (map[0][0] instanceof CornerTile)
			map[0][0].changeImage("brInCorner.png");
		if (map[0][map[1].length - 1] instanceof CornerTile)
			map[0][map[1].length - 1].changeImage("blInCorner.png");
		if (map[map.length - 1][0] instanceof CornerTile)
			map[map.length - 1][0].changeImage("trInCorner.png");
		if (map[map.length - 1][map[1].length - 1] instanceof CornerTile)
			map[map.length - 1][map[1].length - 1]
					.changeImage("tlInCorner.png");

		// Top Edge
		for (int y = 1; y < map[0].length - 1; y++)
			if (map[0][y] instanceof WallTile)
				map[0][y].changeImage("bWall.png");
			else if (map[0][y] instanceof CornerTile) {
				if (map[0][y + 1] instanceof NormalTile)
					map[0][y].changeImage("brInCorner.png");
				else
					map[0][y].changeImage("blInCorner.png");
			}

		// Bottom Edge
		for (int y = 1; y < map[0].length - 1; y++)
			if (map[map.length - 1][y] instanceof WallTile)
				map[map.length - 1][y].changeImage("tWall.png");
			else if (map[map.length - 1][y] instanceof CornerTile) {
				if (!(map[map.length - 1][y - 1] instanceof NormalTile))
					map[map.length - 1][y].changeImage("trInCorner.png");
				else
					map[map.length - 1][y].changeImage("tlInCorner.png");
			}

		// Left Edge
		for (int x = 1; x < map.length - 1; x++)
			if (map[x][0] instanceof WallTile)
				map[x][0].changeImage("rWall.png");
			else if (map[x][0] instanceof CornerTile) {
				if (map[x - 1][1] instanceof NormalTile)
					map[x][0].changeImage("trInCorner.png");
				else if (map[x + 1][1] instanceof NormalTile)
					map[x][0].changeImage("brInCorner.png");
			}

		// Right Edge
		for (int x = 1; x < map.length - 1; x++)
			if (map[x][map[1].length - 1] instanceof WallTile)
				map[x][map[1].length - 1].changeImage("lWall.png");
			else if (map[x][map[1].length - 1] instanceof CornerTile) {
				if (map[x - 1][map[1].length - 2] instanceof NormalTile)
					map[x][map[1].length - 1].changeImage("trInCorner.png");
				else if (map[x + 1][map[1].length - 2] instanceof NormalTile)
					map[x][map[1].length - 1].changeImage("brInCorner.png");
			}

		return map;
	}

	public ArrayList<GameCharacter> getCreatureList() {
		ArrayList<GameCharacter> creatures = new ArrayList<GameCharacter>();
		creatures.addAll(badList);
		creatures.addAll(goodList);

		return creatures;
	}

	public List<GameCharacter> getFriendlyList() {
		return goodList;
	}

	public List<GameCharacter> getEnemyList() {
		return badList;
	}

	public List<GameItem> getItemList() {
		return itemList;
	}
	public List<GameCharacter> getPlaceHolders(){
		return placeholders;
	}
	public String getDescription() {
		return description;
	}

	public WinningCondition getWinCondition() {
		return win;
	}

	public Tile[][] getMap() {
		return map;
	}
}
