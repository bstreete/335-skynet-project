package maps;

import items.GameItem;

import java.io.Serializable;

import characters.GameCharacter;
import characters.GameObject;

/**
 * hiddenByFog: If this boolean is true, the tile is not currently visible by
 * the player's characters. Bot controlled characters will spawn here if true.
 * If it is false, then a player owned character currently has eyes on that
 * tile.
 * 
 * seenByPlayer: If this is true, the tile has been seen by a player character
 * in the past. This means that the terrain is visible in one form or another
 * regardless if a player character can see it now. If it is false, that tile is
 * unexplored, and will not be visible in any way.
 * 
 * inRange: If this is true, the currently selected character's ability can be
 * used against this square. Used by the display to highlight targetable tiles.
 */
public abstract class Tile implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean accessible, blocksSight, hiddenByFog, seenByPlayer,
			inRange;
	private String imageFile;
	private GameObject occupant;

	public Tile(boolean accessible, boolean blocksSight, String filename) {
		this.accessible = accessible;
		this.blocksSight = blocksSight;
		this.imageFile = filename;
		hiddenByFog = true;
		seenByPlayer = false;
		inRange = false;
	}

	public boolean getInRange() {
		return inRange;
	}

	public void setInRange(boolean set) {
		inRange = set;
	}

	public boolean isAccessible() {
		return accessible;
	}

	public void setAccessible(boolean accessible) {
		this.accessible = accessible;
	}

	public boolean isBlocksSight() {
		return blocksSight;
	}

	public void setBlocksSight(boolean visible) {
		this.blocksSight = visible;
	}

	public String getImageFile() {
		return imageFile;
	}

	public void changeImage(String file) {
		imageFile = file;
	}

	public GameObject getOccupant() {
		return occupant;
	}

	// called when character steps on tile
	public void setOccupant(GameObject newOccupant) {
		occupant = newOccupant;
	}
	public void onStepOn(GameCharacter character){
		if (this.getOccupant() instanceof GameItem) {
			character.getItems().add((GameItem) this.getOccupant());
		}
		this.setAccessible(false); // occupy new tile
		this.setOccupant(character);
		this.stepOnEffect(character);
	}
	public abstract void stepOnEffect(GameCharacter character);
	public void stepOff(){
		this.setOccupant(null);
		this.setAccessible(true);
	}
	public boolean getSeenByPlayer() {
		return seenByPlayer;
	}

	public boolean getHiddenByFog() {
		return hiddenByFog;
	}

	public void setSeenByPlayer() {
		seenByPlayer = true;
	}

	public void setHiddenByFog(boolean seen) {
		hiddenByFog = seen;
	}
}
