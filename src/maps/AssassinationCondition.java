package maps;

import java.util.ArrayList;
import java.util.List;

import characters.GameCharacter;

public class AssassinationCondition implements WinningCondition {
	private static final long serialVersionUID = -7422662990624963060L;

	private List<GameCharacter> goodList;
	private GameCharacter badKing;

	public AssassinationCondition(List<GameCharacter> goodList, GameCharacter badKing) {
		this.goodList = goodList;
		this.badKing = badKing;
	}

	@Override
	public boolean checkWin() { // you win if you kill their king
		return badKing.isDead();
	}

	@Override
	public boolean checkLose() { // you loose if your units die
		return goodList.isEmpty();
	}

}
