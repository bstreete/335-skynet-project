package maps;

import java.util.ArrayList;

import characters.GameObject;

public class CustomMapFactory {
	private static final char NORMAL = 'N';
	private static final char WALL = 'W';
	private static final char EMPTY = 'E';
	private static final char INNERCORNER = 'C';
	private static final char OUTERCORNER = 'O';

	private static final char SURVIVALTILE = 'S';
	private static final char HEAL = 'H';
	private static final char TRAP = 'T';

	// game character code
	private static final String ZOMBIE = "Zombie";
	private static final String ITEMZOMBIE = "Item Zombie";
	private static final String STRONGZOMBIE = "Strong Zombie";
	private static final String KINGZOMBIE = "King Zombie";
	private static final String RIFLEMAN = "Rifleman";
	private static final String MEDIC = "Medic";
	private static final String CAPTAIN = "Captain";
	private static final String ASSAULT = "Assault";
	private static final String MACHINEGUNNER = "Gunner";
	private static final String GUNMAN = "Gunman";
	// game item code
	private static final String ARMOR = "Armor";
	private static final String BOOTS = "Overdrive";
	private static final String PAINKILLER = "Painkiller";
	private static final String CAFFEINE = "Optics";
	private static final String TESTOSTERONE = "Stimulant";
	// other
	private static final String DOOR = "Door";
	private static final String PLACEHOLDER = "PlaceHolder";

	// winning condition code

	/**
	 * Turns map into String representation which can be stored in text file
	 * will also account for game objects !Assumes that the win condition is
	 * Elimination!
	 */
	public static String turnToMapText(Tile[][] map) {
		ArrayList<GameObject> objectList = new ArrayList<GameObject>();
		StringBuilder text = new StringBuilder(10000);
		text.append(map.length + "\n" + map[0].length + "\n");
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				if (map[i][j].getOccupant() != null) {
					objectList.add(map[i][j].getOccupant());
				}
				Tile tile = map[i][j];
				if (tile instanceof NormalTile) {
					text.append("N");
				} else if (tile instanceof HealTile) {
					text.append("H");
				} else if (tile instanceof TrapTile) {
					text.append("T");
				} else if (tile instanceof WallTile) {
					text.append("W");
				} else {
					text.append("E");
				}
			}
			text.append("\n");
		}
		for (GameObject go : objectList) {
			text.append(go.getRow() + "\n" + go.getCol() + "\n");
			switch (go.getName()) {
			case ZOMBIE:
				text.append("ZOMBIE\n");
				break;
			case ITEMZOMBIE:
				text.append("ITEMZOMBIE\n");
				break;
			case STRONGZOMBIE:
				text.append("STRONGZOMBIE\n");
				break;
			case KINGZOMBIE:
				text.append("KINGZOMBIE\n");
				break;
			case RIFLEMAN:
				text.append("RIFLEMAN\n");
				break;
			case MEDIC:
				text.append("MEDIC\n");
				break;
			case CAPTAIN:
				text.append("CAPTAIN\n");
				break;
			case ASSAULT:
				text.append("ASSAULT\n");
				break;
			case MACHINEGUNNER:
				text.append("MACHINEGUNNER\n");
				break;
			case GUNMAN:
				text.append("GUNMAN\n");
				break;
			case ARMOR:
				text.append("ARMOR\n");
				break;
			case BOOTS:
				text.append("BOOTS\n");
				break;
			case PAINKILLER:
				text.append("PAINKILLER\n");
				break;
			case CAFFEINE:
				text.append("CAFFEINE\n");
				break;
			case TESTOSTERONE:
				text.append("TESTOSTERONE\n");
				break;
			case DOOR:
				text.append("DOOR\n");
				break;
			case PLACEHOLDER:
				text.append("PLACEHOLDER\n");
				break;
			default:
			}
		}
		text.append("STOP\nELIMINATION\n");
		return text.toString();
	}
}
