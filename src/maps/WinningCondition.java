package maps;

import java.io.Serializable;

public interface WinningCondition extends Serializable{
	public boolean checkWin();
	public boolean checkLose();
}
