package maps;

import characters.GameCharacter;

/* This tile is used in the Survival mode.
 * If a player unit occupies this tile, then
 * he wins.
 * */

public class SurvivalTile extends Tile {

	public SurvivalTile() {
		// @TODO: change the image when appropriate image is found.
		super(true, false, "survivalTile.png");
	}

	@Override
	public void stepOnEffect(GameCharacter character) {
		// TODO Auto-generated method stub
		
	}

}
