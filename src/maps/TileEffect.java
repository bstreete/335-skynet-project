package maps;

import java.io.Serializable;

public interface TileEffect extends Serializable{
	public void execute();
}
