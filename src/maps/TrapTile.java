package maps;

import soundplayer.SoundPlayer;
import characters.GameCharacter;

/**
 * 
 * disguises as normal tile until someone steps on it
 * damages first person to step on it
 *
 */
public class TrapTile extends Tile {

	private boolean steppedOn;
	public TrapTile() {
		super(true, false, "normalTile.png");
		// TODO Auto-generated constructor stub
		steppedOn=false;
	}

	@Override
	public void stepOnEffect(GameCharacter character) {
		if(!steppedOn){
			character.changeHealth(-500);//takes 500 damage to character who steppe on it
			//can add sound here
			SoundPlayer.playFile("./SkynetSounds/explosion1.mp3");
			this.changeImage("trapTile.png");//change to different tile image here
			steppedOn=true;
		}
	}

}
