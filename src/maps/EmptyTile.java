package maps;

import characters.GameCharacter;

public class EmptyTile extends Tile {
	private static final long serialVersionUID = 2488876284785174021L;

	public EmptyTile() {
		super(false, false, "emptyTile.jpeg");
	}

	@Override
	public void stepOnEffect(GameCharacter character) {
		// TODO Auto-generated method stub

	}

}
