package maps;

import java.util.List;

import characters.GameCharacter;
import characters.GameObject;
import characters.playable.PlayableCharacter;

/* This condition is used for the Survival mode.
 * The player wins if one of their units occupy
 * the survival tile. In this level, there are many
 * strong zombies. It should be a bloodbath. The player
 * loses if all of his units are dead.
 * */

public class SurvivalCondition implements WinningCondition {
	private static final long serialVersionUID = 7197619831364366591L;

	List<GameCharacter> goodList;
	List<SurvivalTile> tiles;

	public SurvivalCondition(List<GameCharacter> goodList, List<SurvivalTile> tile) {
		this.goodList = goodList;
		this.tiles = tile;
	}

	@Override
	public boolean checkWin() {
		for (Tile s : tiles) {
			GameObject occupant = s.getOccupant();
			if (occupant instanceof PlayableCharacter)
				return true;
		}
		return false;
	}

	@Override
	public boolean checkLose() {
		return goodList.isEmpty();
	}

}
