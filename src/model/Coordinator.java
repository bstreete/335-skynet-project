package model;

import items.GameItem;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.DefaultListModel;

import maps.LevelFactory;
import maps.Tile;
import maps.WinningCondition;
import soundplayer.SoundPlayer;
import ai.Decision;
import characters.GameCharacter;
import characters.GameObject;
import characters.playable.PlayableCharacter;
import characters.props.Corpse;
import characters.unplayable.StrongZombie;
import characters.unplayable.UnplayableCharacter;
import display.MainDisplay;

public class Coordinator extends Observable implements Serializable {
	private static final long serialVersionUID = 1597402137819645758L;

	// Instance Variables
	private String title, description;
	private TurnOrder turnOrder;
	private Tile[][] map;
	Queue<Point> shortestPath;
	private List<GameCharacter> creatureList, friendlyList, enemyList,
			corpseList, livingList, placeholders;
	private List<GameItem> itemList;
	private WinningCondition winCondition;
	private int difficulty; // This counter sets the zombie spawn rate frequency
	private int spawnRound; // Every int difficulty rounds, zombie is spawned
	private boolean stepSound = false; // determines which step sound is used;
										// step1 or step2

	public Coordinator(String name, String description, String mapFile) {
		creatureList = new CopyOnWriteArrayList<GameCharacter>();
		livingList = new CopyOnWriteArrayList<GameCharacter>();
		updateLevel(name, description, mapFile);
		spawnRound = 0;
		difficulty = 0;
	}

	/**
	 * Takes the passed name, description, and map file to create a new map by
	 * reading the passed files. Initializes all of the coordinator's objects,
	 * and starts the turn cycle.
	 */
	public void updateLevel(String name, String description, String mapFile) {
		// Load the map
		LevelFactory factory = new LevelFactory();
		map = factory.makeLevel(description, mapFile);

		// Get item/character lists
		livingList = factory.getCreatureList();
		friendlyList = factory.getFriendlyList();
		enemyList = factory.getEnemyList();
		itemList = factory.getItemList();
		placeholders = factory.getPlaceHolders();
		corpseList = new CopyOnWriteArrayList<GameCharacter>();

		creatureList.addAll(livingList);
		turnOrder = new TurnOrder(livingList);
		this.title = name;
		this.description = factory.getDescription();
		winCondition = factory.getWinCondition();

		// Determine what's visible
		updateVisibility();

	}

	/**
	 * Checks to see if the tiles on the map have been seen by the player's
	 * characters. If they have, updates their seenByPlayer tag. This tag
	 * determines if a black square is drawn, or if the proper tileset is drawn.
	 * 
	 * Uses the line of sight algorithm implemented by Kris.
	 */
	public void updateVisibility() {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				map[row][col].setHiddenByFog(true);

				// See if any player character can see the tile
				if (checkCharacterVisibility(row, col)) {
					map[row][col].setHiddenByFog(false);
					map[row][col].setSeenByPlayer();
				}
			}
		}
	}

	/**
	 * Checks to see if any player controlled character can see the specified
	 * tile. Returns true if a character can see the tile.
	 */
	private boolean checkCharacterVisibility(int row, int col) {

		for (GameCharacter c : friendlyList) {
			if (c.hasLineOfSight(map, row, col)) {
				map[row][col].setHiddenByFog(false);
				map[row][col].setSeenByPlayer();
				return true;
			}
		}

		return false;
	}

	/**
	 * Used to update the tiles and determine what tiles the currently selected
	 * character can use his ability on.
	 * 
	 * If the boolean is true, this method checks against the range of the
	 * character. Otherwise, checks against mobility.
	 */

	public void updateRange(GameCharacter current, boolean action) {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {

				// If the character has line of sight, and the target is in
				// range.
				if (action) {

					if (current.hasLineOfSight(map, row, col)
							&& current.rangeFrom(map, row, col) <= current
									.getRange())
						map[row][col].setInRange(true);
					else
						map[row][col].setInRange(false);
				}
				// If the character is moving
				else {
					if (PathFinder.getShortestPath(current, row, col, map) != null && map[row][col].isAccessible())
						if (PathFinder.getShortestPath(current, row, col, map)
								.size() <= current.getMobility() + 1)
							map[row][col].setInRange(true);

						else
							map[row][col].setInRange(false);
					else
						map[row][col].setInRange(false); // can't move to inaccessible tiles
				}
			}
		}
	}

	/**
	 * Takes the passed character and determines if it is his turn or not. If it
	 * is, updates the visible tiles and cycles to the next character. If the
	 * character is a bot, calls botTurn to handle determining his moves. If the
	 * game is over, stops calling other methods. Calls notifyObservers to
	 * update the view.
	 * 
	 * Calls notifyObservers to have the GUI refresh.
	 */
	public void nextTurn(GameCharacter selected) {
		Message response;

		// If the game isn't over
		if (!winCondition.checkWin() && !winCondition.checkLose()) {
			if (verifyCurrentTurn(selected)) {
				turnOrder.nextTurn();
				response = Message.NextTurn;

				selected = turnOrder.getCurrentTurn();

				if (selected instanceof UnplayableCharacter)
					botTurn();

			} else
				response = Message.WrongTurn;
		}

		// The game is over
		else {
			if (winCondition.checkWin())
				response = Message.GameWon;
			else
				response = Message.GameLost;

		}

		// Add a zombie every int difficulty turns!
		difficulty = MainDisplay.getSpawnCounterMultiplier()
				* (friendlyList.size() + enemyList.size());
		spawnZombie(difficulty);

		this.notifyObservers(response);

	}

	/**
	 * Handles the decision making for AI characters. Uses the Decision class to
	 * determine what the passed character should do. After the bot's turn,
	 * cycles to the next character which may or may not be a bot as well.
	 */
	private void botTurn() {

		GameCharacter activeBot = turnOrder.getCurrentTurn();
		if (activeBot instanceof UnplayableCharacter) {
			Decision d = ((UnplayableCharacter) activeBot).getDecision(
					friendlyList, enemyList, map);
			Point coords = d.getTarget();

			// If the bot can move
			if (coords.x != -1) {
				GameCharacter target = getCharacterAt(coords);

				// checks if gamecharacter is at coordinates
				if (turnOrder.canAct() && target != null
						&& activeBot.canAttackAt(coords.x, coords.y, map)) {
					performAction(activeBot, coords);
					Timer t = new Timer();
					t.schedule(new ActionTimer(), 300);

					// If the character hasn't already moved
				} else if (turnOrder.canMove() && target == null) {
					moveCharacter(activeBot, coords);
				}// The character has moved and can't attack
				else
					nextTurn(activeBot);
			} else
				nextTurn(activeBot);
		}
	}

	/**
	 * Handles moving the chosen character to a new coordinate. Starts by
	 * verifying it is the passed character's turn. If it is, makes sure he
	 * hasn't already moved. If he hasn't, verifies he can move to the
	 * destination. If he can, the character is moved successfully. The
	 * visibility of the map tiles is updated after a successful move.
	 * 
	 * Calls notifyObservers afterwards to update the user interface.
	 */
	public void moveCharacter(GameCharacter selected, Point destination) {
		Message response;
		List<Point> tempPath = PathFinder.getShortestPath(selected,
				destination.x, destination.y, map);

		if (!verifyCurrentTurn(selected))
			response = Message.WrongTurn;

		else if (!turnOrder.canMove())
			response = Message.AlreadyMoved;

		else if (tempPath == null
				|| tempPath.size() > selected.getMobility() + 1)
			response = Message.CannotMove;
		else if (!map[destination.x][destination.y].isAccessible())
			response = Message.CannotMove;

		else {
			map[selected.getRow()][selected.getCol()].stepOff();
			shortestPath = PathFinder.getShortestPath(selected, destination.x,
					destination.y, map);

			moveSingleTile(shortestPath.poll());

			turnOrder.moved();

			SoundPlayer.playFile("./SkynetSounds/step1.mp3");

			response = Message.Moving;
		}

		// If the bot fails to move, try again
		if (selected instanceof UnplayableCharacter
				&& response != Message.Moving) {

			botTurn();

		}
		this.notifyObservers(response);
	}

	/**
	 * Moves the current character a single tile along its movement path. Used
	 * for graphics purposes. If there is another tile along the path before the
	 * destination, a timer is started that expires after 12 frames. When the
	 * timer expires, this method is called again to move the character further
	 * down the path.
	 * 
	 * If the current location is the destination, stops the cycle and notifies
	 * observers.
	 */
	private void moveSingleTile(Point next) {

		GameCharacter mover = turnOrder.getCurrentTurn();

		mover.move(next.x, next.y, map);

		if (shortestPath.peek() != null) {
			notifyObservers(Message.Moving);
			Timer t = new Timer();
			t.schedule(new WalkTimer(), 300);
		}

		else {
			map[next.x][next.y].onStepOn(mover);
			updateVisibility();
			notifyObservers(Message.MoveSuccessful);

			if (mover instanceof UnplayableCharacter)
				botTurn();
		}
	}

	/**
	 * Takes the passed character and checks if it is his turn. If it is, checks
	 * to see if the target is a valid point to act on. If it is, checks to see
	 * if the current character has already moved. If it hasn't, performs the
	 * action.
	 * 
	 * Afterwards, checks to see if the target died. If it did, removes the
	 * object from the corresponding lists of characters to update turn orders
	 * and the win conditions.
	 * 
	 * Notifies observers after processing.
	 */
	public void performAction(GameCharacter selected, Point location) {

		Message response = Message.ActionSuccessful;

		GameObject target = map[location.x][location.y].getOccupant();

		// If the target isn't another gameCharacter
		if (!(target instanceof GameCharacter))
			response = Message.InvalidTarget;

		// If the character isn't the active character
		else if (!verifyCurrentTurn(selected))
			response = Message.WrongTurn;

		// If the character already acted this turn
		else if (!turnOrder.canAct())
			response = Message.AlreadyActed;

		// If the character can't see the target tile
		else if (!selected.hasLineOfSight(map, location.x, location.y))
			response = Message.OutOfSight;

		// If the target is out of range
		else if (selected.rangeFrom(map, location.x, location.y) > selected
				.getRange() + 1)
			response = Message.OutOfRange;

		// If the character can't target the chosen character
		else if (!selected.actOn((GameCharacter) target, map))
			response = Message.InvalidTarget;

		// Otherwise, successfully used ability on the target
		else {
			turnOrder.acted();

			// Check to see if the target is dead
			if (((GameCharacter) target).isDead()) {
				Corpse newCorpse = new Corpse(
						((GameCharacter) target).getName(), target.getRow(),
						target.getCol(),
						((GameCharacter) target).getImagePrefix(),
						MainDisplay.deathImages.get(((GameCharacter) target)
								.getImagePrefix()),
						((GameCharacter) target).getLeftFacing());

				creatureList.remove(target);
				livingList.remove(target);
				corpseList.add(newCorpse);
				creatureList.add(newCorpse);

				// If a player owned character, remove from the good list and
				// update visibility
				if (target instanceof PlayableCharacter) {
					friendlyList.remove(target);
					updateVisibility();

					// If a bot character, update the enemy list
				} else
					enemyList.remove(target);

				if (winCondition.checkWin()) {
					response = Message.GameWon;
					this.notifyObservers(response);
				} else if (winCondition.checkLose()) {
					response = Message.GameLost;
					this.notifyObservers(response);
				}

				map[location.x][location.y].setOccupant(null);
				map[location.x][location.y].setAccessible(true);

				response = Message.TargetDestroyed;
			}

			// Only display win/lose if the player is acting to prevent message
			// spam
			if (selected instanceof PlayableCharacter) {
				// Check to see if the game is won/lost
				if (winCondition.checkWin())
					response = Message.GameWon;
				else if (winCondition.checkLose())
					response = Message.GameLost;
			}
		}

		this.notifyObservers(response);
	}

	/**
	 * Compares the selected character to the currently active character. If the
	 * characters are the same, returns true. Otherwise, returns false.
	 */
	private boolean verifyCurrentTurn(GameCharacter selected) {
		if (selected != turnOrder.getCurrentTurn())
			return false;
		else
			return true;
	}

	private class ActionTimer extends TimerTask {
		@Override
		public void run() {
			botTurn();
		}
	}

	private class WalkTimer extends TimerTask {

		@Override
		public void run() {

			if (stepSound == false) {
				stepSound = true;
				SoundPlayer.playFile("./SkynetSounds/step2.mp3");
			} else {
				SoundPlayer.playFile("./SkynetSounds/step1.mp3");
				stepSound = false;
			}
			moveSingleTile(shortestPath.poll());
		}

	}

	/**
	 * Gets the occupant of the specified point, if it's empty returns null.
	 */
	private GameCharacter getCharacterAt(Point location) {
		if (map[location.x][location.y].getOccupant() instanceof GameCharacter)
			return (GameCharacter) map[location.x][location.y].getOccupant();
		else
			return null;
	}

	public void updateCreatureList() {
		creatureList.clear();

		for (GameCharacter c : friendlyList)
			creatureList.add(c);

		for (GameCharacter c : enemyList)
			creatureList.add(c);
		livingList.clear();
		corpseList.clear();
		for (GameCharacter c : creatureList) {
			if (c.isDead()) {
				corpseList.add(c);
			} else
				livingList.add(c);
		}
	}

	/*
	 * Zombie spawner. Returns false if zombie failed to spawn. This can occur
	 * if all valid spaces a zombie can spawn are visible.
	 */

	private boolean spawnZombie(int difficulty) {
		if (spawnRound < difficulty) {
			spawnRound++;
			return true;
		} else if (enemyList.size() < friendlyList.size()) {
			Point temp = getClosestNotVisibleTile();
			if (temp != null) {
				GameCharacter zed = new StrongZombie(temp.x, temp.y);

				// Set animations for the new character
				zed.setImagePrefix("strongZombie");
				zed.setAnimationMap(MainDisplay.characterImages.get(zed
						.getImagePrefix()));

				enemyList.add(zed);
				creatureList.add(zed);
				turnOrder.addCharacter(zed);

				map[temp.x][temp.y].setOccupant(zed);
				map[temp.x][temp.y].setAccessible(false);
			}
			SoundPlayer.playFile("./SkynetSounds/monsterMovement.mp3");
			spawnRound = 0;
			return true;
		}
		return false;
	}

	// Getters and Setters

	private Point getClosestNotVisibleTile() {

		List<Point> notVisible = new ArrayList<Point>();

		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				if (!checkCharacterVisibility(row, col)
						&& map[row][col].isAccessible()) {
					notVisible.add(new Point(row, col));
				}
			}
		}

		if (notVisible.size() == 0)
			return null;

		int minDistance = 999;
		Point current = null;

		for (Point p : notVisible) {
			for (GameCharacter guy : friendlyList) {
				int temp = Math.abs(guy.getRow() - p.x)
						+ Math.abs(guy.getCol() - p.y);
				if (temp < minDistance) {
					minDistance = temp;
					current = p;
				}
			}
		}
		return current;
	}

	public Point getNextStep() {
		return shortestPath.peek();
	}

	public Queue<Point> getShortestPath() {
		return shortestPath;
	}

	public GameCharacter getCurrentTurn() {
		return turnOrder.getCurrentTurn();
	}

	public String getDescription() {
		return description;
	}

	public Tile[][] getMap() {
		return map;
	}

	public String getTitle() {
		return title;
	}

	public List<GameCharacter> getCorpseList() {
		return corpseList;
	}

	public List<GameCharacter> getLivingList() {
		return livingList;
	}

	public List<GameCharacter> getCreatureList() {
		return creatureList;
	}

	public List<GameCharacter> getFriendlyList() {
		return friendlyList;
	}

	public List<GameCharacter> getTurnOrder() {
		return turnOrder.getTurnOrder();
	}

	public void setTurnOrder(TurnOrder t) {
		turnOrder = t;
	}

	public DefaultListModel<GameCharacter> getTurnModel() {
		return turnOrder.getTurnModel();
	}

	public GameObject getOccupant(int row, int col) {
		return map[row][col].getOccupant();
	}

	public void setFriendlyList(List<GameCharacter> newFriendlyList) {
		friendlyList = newFriendlyList;
	}

	public List<GameCharacter> getPlaceholders() {
		return placeholders;
	}

	public void setPlaceholders(List<GameCharacter> placeholders) {
		this.placeholders = placeholders;

	}
}
