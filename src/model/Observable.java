package model;

import java.util.HashSet;

abstract class Observable {

	HashSet<Observer> observersList;
	
	Observable(){
		observersList = new HashSet<Observer>();
	}

	public void addObserver(Observer newObserver) {
		observersList.add(newObserver);
	}
	
	public void notifyObservers(Message update) {
		for (Observer o: observersList)
			o.update(update);
	}
}
