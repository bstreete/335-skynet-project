package model;

import java.util.Random;

/**
 * The combat class is responsible for processing combat moves by all creatures,
 * including player controlled, AI controlled, and props.
 * 
 * @author Brayden
 * 
 */
public class Combat {

	/**
	 * Given an attacker creature and defending creature, this method determines
	 * the damage dealt to the target. This method assumes that the attacker and
	 * target have been initialized, have valid attribute values, and are
	 * equipped.
	 * 
	 * @param attacker
	 * @param target
	 * 
	 * @return The damage dealt to the creature. If the damage is 0, the attack missed. 
	 */
	public static int attack() {
		
		Random generator = new Random();
		
		// TODO: Calculate distance for range
		
		// TODO: Get weapon and character attributes
		
		// TODO: Determine if attack lands
		
		// TODO: Successful attack, calculate damage dealt
		
		return 0; 
	}
}
