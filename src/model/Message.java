package model;

public enum Message {

	WrongTurn, AlreadyMoved, AlreadyActed, OutOfRange, TargetDestroyed, 
	MoveSuccessful, ActionSuccessful, CannotMove, InvalidTarget, NextTurn, 
	GameWon, GameLost, OutOfSight, Moving, Start;
	

	public String toString() {
		switch (this) {
		case Start:
			return ("Game initialized.");
		case WrongTurn:
			return ("This character is not active.");
		case AlreadyActed:
			return ("This character has already performed an action.");
		case AlreadyMoved:
			return ("This character has already moved this turn.");
		case CannotMove:
			return ("This character cannot move there.");
		case Moving:
			return ("This character is moving.");
		case InvalidTarget:
			return ("Invalid target.");
		case MoveSuccessful:
			return ("Moved successfully.");
		case OutOfRange:
			return ("Target is out of range.");
		case OutOfSight:
			return ("Target tile is out of sight for this character.");
		case TargetDestroyed:
			return ("Target killed.");
		case NextTurn:
			return ("Next creature's turn.");
		case GameLost:
			return("Game Over");
		case GameWon:
			return("You Won!");
		case ActionSuccessful:
		default:
			return ("Ability performed successfully.");
		}
	}
}
