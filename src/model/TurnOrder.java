package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;

import characters.GameCharacter;

public class TurnOrder implements Serializable {
	private static final long serialVersionUID = 5285902164581226358L;

	private GameCharacter currentTurn;
	private List<GameCharacter> turnOrder;
	private boolean moved, acted;
	private DefaultListModel<GameCharacter> turnModel;

	/**
	 * Combines the lists of characters. Sorts them by mobility with the highest
	 * attribute value at index 0. Ties are randomly sorted. Then adds the
	 * creatures in order to the turnOrder.
	 */
	public TurnOrder(List<GameCharacter> creatures) {

		moved = false;
		acted = false;

		turnOrder = new ArrayList<GameCharacter>();
		turnModel = new DefaultListModel<GameCharacter>();
		turnOrder = creatures;

		Collections.sort(turnOrder, new MobilitySort());
		nextTurn();
	}

	/**
	 * Gets the character at index 0 of the order. Moves the current character
	 * to the last position in the queue. the currentTurn variable will have the
	 * acting creature. Updates the list model used to update the turn panel.
	 */
	public synchronized void nextTurn() {
		currentTurn = turnOrder.remove(0);
		moved = false;
		acted = false;

		turnOrder.add(currentTurn);

		turnModel.clear();
		// Update the Model for graphical output
		for (int x = 0; x < 8; x++)
			turnModel.addElement(turnOrder.get(x % turnOrder.size()));
	}

	/**
	 * Returns true if the character can perform an action. Returns false if the
	 * character has already acted this turn.
	 */
	public boolean canAct() {
		return !acted;
	}

	/**
	 * Returns true if the character can move. Returns false if the character
	 * has already moved this turn.
	 */
	public boolean canMove() {
		return !moved;
	}

	public void acted() {
		acted = true;
	}

	public void moved() {
		moved = true;
	}

	/**
	 * Removes the specified creature from the turn order.
	 */
	public void characterDied(GameCharacter corpse) {
		turnOrder.remove(corpse);
	}

	public DefaultListModel<GameCharacter> getTurnModel() {
		return turnModel;
	}

	/**
	 * Adds the passed creature to the end of the turn order. Used for spawning
	 * new zombies, and if friendly creatures are encountered.
	 */
	public void addCharacter(GameCharacter newCreature) {
		turnOrder.add(newCreature);
	}

	public List<GameCharacter> getTurnOrder() {
		return turnOrder;
	}

	public GameCharacter getCurrentTurn() {
		return currentTurn;
	}
}
