package model;

import java.util.Comparator;
import java.util.Random;

import characters.GameCharacter;

/**
 * Simple comparator that sorts characters by their mobility attribute with the
 * largest mobility listed first. If the characters have the same mobility, it
 * will randomly place one creature before the next.
 */
public class MobilitySort implements Comparator<GameCharacter> {

	@Override
	public int compare(GameCharacter first, GameCharacter second) {

		Random random = new Random();
		int next = random.nextInt();

		if (first.getMobility() > second.getMobility())
			return -1;
		else if (first.getMobility() < second.getMobility())
			return 1;

		// Same attribute value, return random
		if (next % 3 == 0)
			return 0;
		else if (next % 3 == 1)
			return -1;
		else
			return 1;
	}

}
