package model;
import items.GameItem;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import maps.Tile;
import characters.GameCharacter;
import characters.playable.PlayableCharacter;

public class PathFinder {
	/**
	 * This method uses Dijkstra's algorithm to return a shortest path (List of Points)
	 * from a selected character to a destination row and col.
	 */
	public static LinkedList<Point> getShortestPath(GameCharacter selected, int row, int col, Tile[][] map){
		
		Point source = new Point(selected.getRow(),selected.getCol());//source is selected character
		Point destination = new Point(-1,-1);// we do not know if we can reach destination yet
		Point[][] prev = new Point[map.length][map[0].length]; //keeps track of previous points as we traverse through map
		int[][] distance = new int[map.length][map[0].length]; //keeps track of distance traveled as we traverse through map
		ArrayList<Point> Q = new ArrayList<Point>();// List of all points
		initializeHelpers(prev,distance,Q);// set up the helpers
		distance[source.x][source.y]=0; //distance to source should be 0
		
		while(!Q.isEmpty()){
			Point current = removeSmallest(Q,distance); //get and remove point with smallest distance from source
			if(distance[current.x][current.y]==9999999){ //if the smallest distance is infinity, we can assume there is no way to get to destination
				break;
			}
			for(Point v: getNeighbors(current)){ //for each neighbor of the current node
				if(v.x<map.length&&v.x>=0&&v.y<map[0].length&&v.y>=0){
						int alt = distance[current.x][current.y]+1;
						//check if we have reached destination
							if(v.x==row&&v.y==col){
								distance[v.x][v.y]=alt;
								prev[v.x][v.y]=current;
								destination=new Point(v.x,v.y);//if we have end search
								break;
							}
							else if(map[v.x][v.y].isAccessible()){ //else check if it is accessible
								if(alt< distance[v.x][v.y]){
									
									distance[v.x][v.y]=alt;
									prev[v.x][v.y]=current;
									if(alt>selected.getMobility()){ //stop searching if min distance is greater than mobility
										return null;
									}
								}
							}
							else{} //if not accessible, then ignore
				}
			}
			if(destination.x!=-1){
				break; //end loop if we found destination
			}
		}
		Point cur=destination;
		if(destination.x==-1){
			return null;
		}
		LinkedList<Point> path = new LinkedList<Point>(); //make a list of all points along path
		while(cur.x!=source.x||cur.y!=source.y){
			path.add(new Point(cur.x,cur.y)); //add points into list
			cur= prev[cur.x][cur.y]; //get the previous point along the path
		}
		path.add(source); //add source to path
		Collections.reverse(path); //reverse the order so we begin at source and end at destination
		return path;
	}
	/**
	 * This method returns a list of surrounding points
	 * 
	 */
	public static ArrayList<Point> getNeighbors(Point p){
		ArrayList<Point> neighbors= new ArrayList<Point>(8);
		
		neighbors.add(new Point(p.x+1,p.y+1));
		neighbors.add(new Point(p.x+1,p.y-1));
		neighbors.add(new Point(p.x-1,p.y+1));
		neighbors.add(new Point(p.x-1,p.y-1));
		neighbors.add(new Point(p.x+1,p.y));
		neighbors.add(new Point(p.x,p.y+1));
		neighbors.add(new Point(p.x-1,p.y));
		neighbors.add(new Point(p.x,p.y-1));
		
		return neighbors;
	}
	/**
	 * Given a list of points and its distance from a source,
	 * this method removes and returns the point with the smallest distance
	 */
	public static Point removeSmallest(ArrayList<Point> Q, int[][]distance){
		int min= 9999999;
		int index=-1;
		for(int i =0; i< Q.size();i++){
			Point node =Q.get(i);
			if(distance[node.x][node.y]<=min){
				min=distance[node.x][node.y];
				index=i;
			}
		}
		return Q.remove(index);	
	}
	/**
	 * This method sets up the helpers for the Dijkstra's algorithm
	 */
	public static void initializeHelpers(Point[][] prev, int[][] distance, List<Point> Q){
		for(int i =0; i<prev.length; i++){ 
			for(int j=0; j<prev[0].length; j++){ 
				prev[i][j] =null;// set each previous point to null
				distance[i][j]=9999999; //set each distance to infinity
				Q.add(new Point(i,j)); //add point to list
			}
		}
	}
	/**
	 * This method checks to see if selected character can attack or move at given coordinates
	 */
	public static boolean checkValidMove(GameCharacter selected, int row, int col, Tile[][]map){
		if(row<0 ||col<0|| row>=map.length||col>=map[0].length){
			return false;
		}
		if(map[row][col].getOccupant()==null){
			return selected.canMoveTo(row, col, map);//return true if you can move to that empty tile		
		}
		else{
			if(map[row][col].getOccupant() instanceof GameItem){
				return selected.canMoveTo(row, col, map); //return true if you can move to tile with item
			}
			else{
				return selected.canAttackAt(row, col, map) && ((GameCharacter) map[row][col].getOccupant())instanceof PlayableCharacter; //return true if you can attack opponent
			}
		}
	}
}
