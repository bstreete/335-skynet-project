package model;

import items.GameItem;

import java.util.Comparator;

/**
 * Sorts the passed items in alphabetical order ignoring cases. 
 */

public class GameItemStringSort implements Comparator<GameItem>{

	@Override
	public int compare(GameItem first, GameItem second) {
		
		return first.getName().compareToIgnoreCase(second.getName());
	}
}
