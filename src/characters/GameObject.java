package characters;

import java.io.Serializable;

public abstract class GameObject implements Serializable {
	private int row, col;
	private String imageFile, description, name;

	public GameObject(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public GameObject(String name, int row, int col, String imageFile,
			String description) {
		this.row = row;
		this.col = col;
		this.imageFile = imageFile;
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String newName) {
		name = newName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int newRow) {
		row = newRow;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int newCol) {
		col = newCol;
	}

	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String newImage) {
		imageFile = newImage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String newDesc) {
		description = newDesc;
	}
}
