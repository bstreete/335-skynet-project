package characters.playable;

import ability.NormalAttackAbility;

public class Gunman extends PlayableCharacter {

	public Gunman(int row, int col) {
		super("Gunman", 1000, 5, 6, new NormalAttackAbility(900, "Gunman"), row, col,
				"cqbStationary.png");

		setImagePrefix("cqb");
	}
}
