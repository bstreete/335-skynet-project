package characters.playable;

import ability.NormalAttackAbility;

/**
 * Holds place in map where selected characters will be put in
 * 
 */
public class PlaceHolder extends PlayableCharacter {
	public PlaceHolder(int row, int col) {
		super("PlaceHolder", 0, 0, 0, new NormalAttackAbility(0, ""), row, col, "placeHolder.jpg");
	}
}
