package characters.playable;

import ability.NormalAttackAbility;

public class Rifleman extends PlayableCharacter {

	public Rifleman(int row, int col) {
		super("Rifleman", 1000, 5, 12, new NormalAttackAbility(900, "Rifleman"), row, col,
				"rifleStationary.png");

		setImagePrefix("rifle");
	}
}
