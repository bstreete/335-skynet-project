package characters.playable;

import ability.NormalAttackAbility;

public class MachineGunner extends PlayableCharacter {

	public MachineGunner(int row, int col) {

		super("Gunner", 1000, 5, 8, new NormalAttackAbility(1000, "MachineGunner"), row,
				col, "gunStationary.png");

		setImagePrefix("gun");
	}
}
