package characters.playable;

import ability.NormalAttackAbility;

public class Assault extends PlayableCharacter {

	public Assault(int row, int col) {
		super("Assault", 1200, 5, 4, new NormalAttackAbility(1500, "Assault"), row,
				col, "assaultStationary.png");
		
		setImagePrefix("assault");
	}
}
