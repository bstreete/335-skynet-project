package characters.playable;

import ability.Ability;
import characters.GameCharacter;

/**
 * This sub class refers to characters that can be controlled by the player.
 * Extends the normal game character class. No modifications.
 */
@SuppressWarnings("serial")
public class PlayableCharacter extends GameCharacter {

	public PlayableCharacter(String name, int health, int mobility, int range,
			Ability ability, int row, int col, String imageFile) {
		super(name, health, mobility, range, ability, row, col, imageFile);
	}
}
