package characters.playable;

import ability.HealAbility;

public class Medic extends PlayableCharacter {
	public Medic(int row, int col) {
		super("Medic", 800, 5, 2, new HealAbility(400), row, col, 
				"medicStationary.png");
		
		setImagePrefix("medic");
	}
}
