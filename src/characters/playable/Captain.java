package characters.playable;

import ability.NormalAttackAbility;

public class Captain extends PlayableCharacter {

	public Captain(int row, int col) {

		super("Captain", 1800, 5, 4, new NormalAttackAbility(2000, "Captain"), row, col,
				 "leaderStationary.png");
		setImagePrefix("leader");
	}
}
