package characters.props;

import java.awt.Image;
import java.util.List;

import characters.AnimationState;
import display.MainDisplay;

public class Corpse extends PropCharacter {

	List<Image> deathAnimation;

	public Corpse(String name, int row, int col, String imagePrefix,
			List<Image> deathAnimation, boolean leftFacing) {
		super(row, col);
		setName("Dead " + name);
		setImageFile(imagePrefix + "Dead.png");
		setAnimationState(AnimationState.Dying);
		this.deathAnimation = deathAnimation;
		animationCounter = 0;
		setLeftFacing(leftFacing);
	}

	@Override
	public void incrementAnimationCounter() {
		animationCounter++;
	}
	
	@Override
	public void setAnimationState(AnimationState newState)
	{
		// Do nothing
	}

	/**
	 * Doesn't have to worry about different animation states. Displays the
	 * dying animation, then never stops displaying the basic corpse.
	 */
	@Override
	public Image getCurrentAnimation() {
		if(deathAnimation==null){
			return null;
		}
		if (animationCounter >= deathAnimation.size())
			return MainDisplay.images.get(getImageFile());
		else
			return deathAnimation.get(animationCounter);
	}
}
