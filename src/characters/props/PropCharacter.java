package characters.props;

import characters.GameCharacter;

/**
 * Used to distinguish characters that can be interacted with that have
 * animations, but don't move or attack on their own.
 */
public class PropCharacter extends GameCharacter {

	public PropCharacter(int row, int col) {
		super(row, col);
	}
}
