package characters.props;

import ability.NormalAttackAbility;
import characters.GameCharacter;

/**door object is unplayable by both human and AI.
//However it can be targeted and destroyed to open other rooms
//The AI will only attack the door if the good boolean is true
*/
public class Door extends GameCharacter {

	public Door(int row, int col, boolean good) {
		super("Door",200,0,0,new NormalAttackAbility(0, ""),row, col,"door.png");
		setImagePrefix("gunman");
	}
}
