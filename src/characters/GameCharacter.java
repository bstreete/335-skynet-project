package characters;

import items.GameItem;

import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import characters.props.Door;
import maps.Tile;
import ability.Ability;
import display.MainDisplay;

public abstract class GameCharacter extends GameObject {

	private String imagePrefix;
	private int health, maxHealth, mobility, range;
	protected int animationCounter;
	List<GameItem> items;
	private transient HashMap<AnimationState, List<Image>> animations;
	private AnimationState state;
	private Ability ability;
	private boolean leftFacing;

	public GameCharacter(String name, int health, int mobility, int range,
			Ability ability, int row, int col, String imageFile) {
		super(name, row, col, imageFile, "<html>" + name + "<br>Health: "
				+ health + "/" + health + "<br>Ability: "
				+ ability.getDescription() + "<br>Range: " + range
				+ "<br>Mobility: " + mobility + "</html>");
		this.health = health;
		this.maxHealth = health;
		this.mobility = mobility;
		this.range = range;
		this.ability = ability;
		items = new ArrayList<GameItem>();
		state = AnimationState.Idle;
		animations = new HashMap<AnimationState, List<Image>>();
		animationCounter = 1;
		leftFacing = true;
	}

	public GameCharacter(int row, int col) {
		super(row, col);
	}

	// /////////////////stats

	public int getMaxhealth() {
		return maxHealth;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int h) {
		health = h;
		if (health > maxHealth) {
			health = maxHealth;
		}
	}

	public void setMaxHealth(int h) {
		maxHealth = h;
		health = h;
	}

	public int getRange() {
		return range;
	}

	public void changeRange(int change) {
		range += change;
	}

	public String getAbilityDescription() {
		return ability.getDescription();
	}

	public int getMobility() {
		return mobility;
	}

	public void changeMobility(int change) {
		mobility += change;
	}

	public void setImagePrefix(String prefix) {
		imagePrefix = prefix;
	}

	public String getImagePrefix() {
		return imagePrefix;
	}

	public Ability getAbility() {
		return ability;
	}

	public void setAbility(Ability a) {
		ability = a;
	}

	public List<GameItem> getItems() {
		return items;
	}

	// /////////////////actions
	public boolean actOn(GameCharacter guy, Tile[][] map) {
		if (canAttackAt(guy.getRow(), guy.getCol(), map)) {
			ability.execute(guy); // execute your ability on this guy
			return true;
		}
		return false;
	}

	public boolean canMoveTo(int toRow, int toCol, Tile[][] map) {
		int row = getRow();
		int col = getCol();
		if (map[toRow][toCol].isAccessible()) { // use the private helper
			return canMove(row + 1, col, toRow, toCol, map, getMobility())
					|| // check each direction
					canMove(row + 1, col + 1, toRow, toCol, map, getMobility())
					|| canMove(row, col + 1, toRow, toCol, map, getMobility())
					|| canMove(row - 1, col + 1, toRow, toCol, map,
							getMobility())
					|| canMove(row - 1, col, toRow, toCol, map, getMobility())
					|| canMove(row - 1, col - 1, toRow, toCol, map,
							getMobility())
					|| canMove(row, col - 1, toRow, toCol, map, getMobility())
					|| canMove(row + 1, col - 1, toRow, toCol, map,
							getMobility());
		} else
			return false;
	}

	private boolean canMove(int currentRow, int currentCol, int toRow,
			int toCol, Tile[][] map, int movesLeft) {
		if (currentRow < 0 || currentCol < 0 || currentRow > map.length - 1
				|| currentCol > map[0].length - 1) { // if we are off map return
														// false
			return false;
			// if that tile is not accessible return false
		} else if (!map[currentRow][currentCol].isAccessible()) {
			return false;
			// if we are at destination return true
		} else if (currentRow == toRow && currentCol == toCol) {
			return true;
			// if we are not at definition
		} else {
			// if we have no more moves left
			if (movesLeft <= 0) {
				return false;
			} else {
				// consider each possible movements, vertical, horizontal,
				// diagonal
				return canMove(currentRow + 1, currentCol, toRow, toCol, map,
						movesLeft - 1)
						|| canMove(currentRow + 1, currentCol + 1, toRow,
								toCol, map, movesLeft - 1)
						|| canMove(currentRow, currentCol + 1, toRow, toCol,
								map, movesLeft - 1)
						|| canMove(currentRow - 1, currentCol + 1, toRow,
								toCol, map, movesLeft - 1)
						|| canMove(currentRow - 1, currentCol, toRow, toCol,
								map, movesLeft - 1)
						|| canMove(currentRow - 1, currentCol - 1, toRow,
								toCol, map, movesLeft - 1)
						|| canMove(currentRow, currentCol - 1, toRow, toCol,
								map, movesLeft - 1)
						|| canMove(currentRow + 1, currentCol - 1, toRow,
								toCol, map, movesLeft - 1);
			}
		}
	}

	public boolean canAttackAt(int toRow, int toCol, Tile[][] map) {
		return ((rangeFrom(map, toRow, toCol) <= range)
				&& hasLineOfSight(map, toRow, toCol) && map[toRow][toCol]
					.getOccupant() != null);
	}

	public boolean hasLineOfSight(Tile[][] map, int startX, int startY) {
		// create a list using Bresenham algorithm
		ArrayList<Tile> tilesInLine = new ArrayList<Tile>();

		int endX = getRow();
		int endY = getCol();
		int ystep, xstep;
		int error;
		int y = endY, x = endX;
		double ddy, ddx;
		int dx = startX - endX;
		int dy = startY - endY;
		if (endY > 0 && endX > 0 && endY < map[0].length && endX < map.length)
			tilesInLine.add(map[endX][endY]);
		if (dy < 0) {
			ystep = -1;
			dy = -dy;
		} else
			ystep = 1;
		if (dx < 0) {
			xstep = -1;
			dx = -dx;
		} else
			xstep = 1;
		ddy = 2 * dy;
		ddx = 2 * dx;
		if (ddx >= ddy) {
			error = dx; //
			for (int i = 0; i < dx; i++) {
				x += xstep;
				error += ddy;
				if (error > ddx) {
					y += ystep;
					error -= ddx;
				}
				// game=point(game, y,x,2);
				if (y > 0 && x > 0 && y < map[0].length && x < map.length)
					tilesInLine.add(map[x][y]);
			}
		}

		else {
			error = dy; //
			for (int j = 0; j < dy; j++) {
				y += ystep;
				error += ddx;
				if (error > ddy) {
					x += xstep;
					error -= ddy;
				}
				if (y > 0 && x > 0 && y < map[0].length && x < map.length)
					tilesInLine.add(map[x][y]);
			}

		}
		//
		// System.out.println("//////////////////////////////");
		//
		// System.out.println("New attack from: " + name);
		// System.out.println("Size of line: " + tilesInLine.size());
		// // check list for tiles that return false visibility
		// System.out.println("Tiles in line: ");

		for (int i = 0; i < tilesInLine.size(); i++) {
			//
			// System.out.println("Step: " + i);
			// System.out.println("Blocks sight: "
			// + ((Tile) tilesInLine.get(i)).isBlocksSight());
			// if (((Tile) tilesInLine.get(i)).getOccupant() != null) {
			// System.out.println("this tile: "
			// + ((Tile) tilesInLine.get(i)).getOccupant());
			// } else
			// System.out.println("this tile: No occupant, tile is "
			// + ((Tile) tilesInLine.get(i)).getImageFile());

			// If this is the last tile in the sequence, sets it as a visible
			// square so the graphics display correctly
			if ( tilesInLine.get(i).isBlocksSight() || tilesInLine.get(i).getOccupant() instanceof Door) {
				if (i == tilesInLine.size() - 1)
					return true;
				else
					return false;
			}
			// System.out.println();
		}
		return true;
	}

	// This method returns the range from a square to this character, measured
	// in squares
	// Usage: GameCharacterObject.rangeFrom(theGameMap, theXValueOfSquare,
	// theYValueOfSquare)

	public int rangeFrom(Tile[][] map, int fromX, int fromY) {
		int range = 0;
		int endY = getCol();
		int endX = getRow();
		int startX = fromX;
		int startY = fromY;

		int ystep, xstep;
		int error;
		int y = endY, x = endX;
		double ddy, ddx;
		int dx = startX - endX;
		int dy = startY - endY;
		if (endY > 0 && endX > 0 && endY < map[0].length && endX < map.length)
			range++;
		if (dy < 0) {
			ystep = -1;
			dy = -dy;
		} else
			ystep = 1;
		if (dx < 0) {
			xstep = -1;
			dx = -dx;
		} else
			xstep = 1;
		ddy = 2 * dy;
		ddx = 2 * dx;
		if (ddx >= ddy) {
			error = dx; //
			for (int i = 0; i < dx; i++) {
				x += xstep;
				error += ddy;
				if (error > ddx) {
					y += ystep;
					error -= ddx;
				}
				// game=point(game, y,x,2);
				if (y > 0 && x > 0 && y < map[0].length && x < map.length)
					range++;
			}
		}

		else {
			error = dy; //
			for (int j = 0; j < dy; j++) {
				y += ystep;
				error += ddx;
				if (error > ddy) {
					x += xstep;
					error -= ddy;
				}
				if (y > 0 && x > 0 && y < map[0].length && x < map.length)
					range++;
			}

		}
		//
		// System.out.println();
		// System.out.println("Range from " + name + ": " + range);
		// System.out.println();

		return range;

	}

	public boolean move(int toRow, int toCol, Tile[][] map) {
		if (canMoveTo(toRow, toCol, map)) {
			setRow(toRow);
			setCol(toCol);
			return true;
		}
		return false;
	}

	public void changeHealth(int change) {
		health += change;
		if (health > maxHealth) {
			health = maxHealth;
		}
	}

	public boolean isDead() {
		return health <= 0;// dead if hp below 0
	}

	// /////////////////location
	public void setAnimationState(AnimationState newState) {
		state = newState;
		if (newState != AnimationState.Moving)
			animationCounter = 0;

	}

	public AnimationState getAnimationState() {
		return state;
	}

	/**
	 * Returns relevant set of animations for the specified character. If the
	 * character is moving, returns the moving set, etc.
	 * 
	 * Returns the static image if a set of animations doesn't exist.
	 */
	public Image getCurrentAnimation() {

		if (animations.containsKey(state) && animations.get(state).size() > 0) {
			int listSize = animations.get(state).size() - 1;

			return animations.get(state).get(animationCounter % listSize);
		}
		// No animation available
		else
			return MainDisplay.images.get(getImageFile());

	}

	public void incrementAnimationCounter() {
		animationCounter++;

		// Last frame of the animation
		if (animations.containsKey(state))
			if (animationCounter == animations.get(state).size()) {

				// Change back to the standard moving animation
				switch (state) {
				case Attacking:
				case Interacting:
					setAnimationState(AnimationState.Moving);
					break;
				case Idle:
				case Moving:
					animationCounter = 0;
				default:
					break;
				}
			}
	}

	public void setAnimationMap(HashMap<AnimationState, List<Image>> newMap) {
		animations = newMap;
	}

	public AnimationState getState() {
		return state;
	}

	public boolean getLeftFacing() {
		return leftFacing;
	}

	public void setLeftFacing(boolean update) {
		leftFacing = update;
	}
}