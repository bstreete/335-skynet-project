package characters.unplayable;

import java.util.List;

import maps.Tile;
import ability.Ability;
import ai.Decision;
import characters.GameCharacter;

/**
 * This class is for distinguishing AI controlled characters from player
 * controlled characters. Creates a getDecision method that determines where the
 * character will move or attack next.
 */
public abstract class UnplayableCharacter extends GameCharacter {

	public UnplayableCharacter(String name, int health, int mobility,
			int range, Ability ability, int row, int col, String imageFile) {
		super(name, health, mobility, range, ability, row, col, imageFile);
	}

	public abstract Decision getDecision(List<GameCharacter> goodList,
			List<GameCharacter> badList, Tile[][] map);

}
