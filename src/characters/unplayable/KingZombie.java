package characters.unplayable;

import java.util.List;

import maps.Tile;
import ability.NormalAttackAbility;
import ai.Decision;
import ai.WaitingAI;
import characters.GameCharacter;

public class KingZombie extends UnplayableCharacter {

	public KingZombie(int row, int col) {
		super("King Zombie", 4000, 4, 2, new NormalAttackAbility(1600, "King Zombie"), row,
				col, "strongZombieIdle1.png");
		setImagePrefix("kingZombie");
	}

	@Override
	public Decision getDecision(List<GameCharacter> goodList,
			List<GameCharacter> badList, Tile[][] map) {
		return WaitingAI.getDecision(this, goodList, badList, map);
	}

}
