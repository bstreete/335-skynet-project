package characters.unplayable;

import java.util.List;

import maps.Tile;
import ability.NormalAttackAbility;
import ai.Decision;
import ai.ItemSeekerAI;
import characters.GameCharacter;

public class ItemZombie extends UnplayableCharacter {

	public ItemZombie(int row, int col) {
		super("Item Zombie", 2500, 3, 2, new NormalAttackAbility(350,
				"Item Zombie"), row, col, "zombieIdle1.png");

		setImagePrefix("zombie");
	}

	@Override
	public Decision getDecision(List<GameCharacter> goodList,
			List<GameCharacter> badList, Tile[][] map) {
		return ItemSeekerAI.getDecision(this, goodList, badList, map);
	}
}
