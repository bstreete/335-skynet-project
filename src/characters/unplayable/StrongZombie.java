package characters.unplayable;

import java.util.List;

import maps.Tile;
import ability.NormalAttackAbility;
import ai.Decision;
import ai.SmartAI;
import characters.GameCharacter;

public class StrongZombie extends UnplayableCharacter {

	public StrongZombie(int row, int col) {
		super("Strong Zombie", 3000, 8, 2, new NormalAttackAbility(500,
				"Strong Zombie"), row, col, "strongZombieIdle1.png");

		setImagePrefix("strongZombie");
	}

	@Override
	public Decision getDecision(List<GameCharacter> goodList,
			List<GameCharacter> badList, Tile[][] map) {
		return SmartAI.getDecision(this, goodList, badList, map);
	}

}
