package characters.unplayable;

import java.util.List;

import maps.Tile;
import ability.NormalAttackAbility;
import ai.Decision;
import ai.RandomAI;
import characters.GameCharacter;

public class Zombie extends UnplayableCharacter {

	public Zombie(int row, int col) {
		super("Zombie", 1500, 3, 2, new NormalAttackAbility(235, "Zombie"), row, col,
				"zombieIdle1.png");

		setImagePrefix("zombie");
	}

	@Override
	public Decision getDecision(List<GameCharacter> goodList,
			List<GameCharacter> badList, Tile[][] map) {
		return RandomAI.getDecision(this, goodList, badList, map);
	}

}
