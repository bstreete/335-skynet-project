package characters;

public enum AnimationState {

	Idle, Moving, Attacking, Dying, Interacting;

	@Override
	public String toString() {
		switch (this) {
		case Attacking:
			return "Attack";
		case Dying:
			return "Death";
		case Idle:
			return "Idle";
		case Interacting:
			return "Interact";
		case Moving:
			return "Walk";
		default:
			return "";
		}
	}
}
