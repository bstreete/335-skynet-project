package display;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import characters.AnimationState;

@SuppressWarnings("serial")
public class MainDisplay extends JFrame {
	// Package private variables - Visible from all other classes inside display
	static final int WINDOWHEIGHT = 800;
	static final int WINDOWWIDTH = 1100;
	static final int INITIALLOCATIONX = 300;
	static final int INITIALLOCATIONY = 80;
	static final int CELLHEIGHT = 32;
	static final int CELLWIDTH = 32;
	static final int REFRESH_RATE = 34;
	static final int EASY = 6;
	static final int MEDIUM = 4;
	static final int HARD = 2;
	static Font menuFont, turnFont, selectedFont, baseFont;
	static Color backgroundColor;
	static final String BASEDIR = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "src"
			+ System.getProperty("file.separator");

	static final String MAPDIR = BASEDIR + "mapText"
			+ System.getProperty("file.separator");

	static final String IMAGEDIR = BASEDIR + "images"
			+ System.getProperty("file.separator");

	static final String TERRAINDIR = IMAGEDIR + "terrain"
			+ System.getProperty("file.separator");

	static final String CHARDIR = IMAGEDIR + "characters"
			+ System.getProperty("file.separator");

	public static HashMap<String, Image> images;
	public static HashMap<String, HashMap<AnimationState, List<Image>>> characterImages;
	public static HashMap<String, List<Image>> deathImages;
	private MenuPanel menuDisplay;
	GamePanel gameDisplay;
	private CustomMapDisplay customDisplay;
	private static int spawnCounter;

	public MainDisplay() {

		// Setup Custom Font
		try {
			baseFont = Font.createFont(Font.TRUETYPE_FONT, new File(IMAGEDIR
					+ "Dodgv2c.ttf"));
			menuFont = baseFont.deriveFont(30f);
			turnFont = baseFont.deriveFont(12f);
			selectedFont = baseFont.deriveFont(16f);

		} catch (FontFormatException e) {
		} catch (IOException e) {
		}
		spawnCounter = EASY;// initial difficulty
		backgroundColor = new Color(32, 32, 32, 255);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(INITIALLOCATIONX, INITIALLOCATIONY);
		this.setSize(WINDOWWIDTH, WINDOWHEIGHT);
		this.addWindowListener(new MainWindowListener());

		menuDisplay = new MenuPanel(this);

		this.add(menuDisplay);
		images = new HashMap<String, Image>();
		characterImages = new HashMap<String, HashMap<AnimationState, List<Image>>>();
		deathImages = new HashMap<String, List<Image>>();
		loadImages(new File(IMAGEDIR));
		organizeCharacterAnimations();
		setUpMenu();
	}

	/**
	 * Menu bar stuff
	 */
	private void setUpMenu() {
		JMenuItem menu = new JMenu("Options");
		// Add two Composites to a Composite
		JMenuItem goBack = new JMenuItem("Back to Menu");
		JMenuItem difficultyNest = new JMenu("Difficulty");
		JMenuItem easy = new JMenuItem("Easy");
		JMenuItem medium = new JMenuItem("Medium");
		JMenuItem hard = new JMenuItem("Hard");
		difficultyNest.add(easy);
		difficultyNest.add(medium);
		difficultyNest.add(hard);
		menu.add(goBack);
		menu.add(difficultyNest);
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		menuBar.add(menu);
		MenuItemListener menuListener = new MenuItemListener();
		goBack.addActionListener(menuListener);
		easy.addActionListener(menuListener);
		medium.addActionListener(menuListener);
		hard.addActionListener(menuListener);
	}

	private class MenuItemListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String text = ((JMenuItem) e.getSource()).getText().trim();
			switch (text) {
			case "Back to Menu":
				if (gameDisplay != null) {
					if (gameDisplay.isVisible()) {
						int response = JOptionPane.showConfirmDialog(null,
								"Would you like to save your game?");

						switch (response) {
						case JOptionPane.NO_OPTION:
							// go back without saving
							gameDisplay.stopMusic();
							returnToMenu();
							break;
						case JOptionPane.CANCEL_OPTION:
							MainDisplay.this
									.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
							break;
						case JOptionPane.YES_OPTION:
							// Save the game, then go back to menu
							gameDisplay.saveGame();
							gameDisplay.stopMusic();
							returnToMenu();
							break;
						}
					}
				}
				break;
			case "Easy":
				spawnCounter = EASY;
				JOptionPane.showMessageDialog(null, "Easy difficulty selected");
				break;
			case "Medium":
				spawnCounter = MEDIUM;
				JOptionPane.showMessageDialog(null,
						"Medium difficulty selected");
				break;
			case "Hard":
				spawnCounter = HARD;
				JOptionPane.showMessageDialog(null, "Hard difficulty selected");
				break;
			default:
			}
		}
	}

	public static int getSpawnCounterMultiplier() {
		return spawnCounter;
	}

	/**
	 * Divides the images into separate hash maps for each character class based
	 * on the image prefixes. Any and all new character types will need a prefix
	 * added to this array or it won't be loaded prior to starting the game.
	 */
	private void organizeCharacterAnimations() {

		String[] characterPrefix = { "zombie", // Zombie
				"gun", // Machine Gunner
				"assault", // Assault
				"cqb", // Gunman
				"leader", // Captain
				"medic", // Medic
				"rifle", // Rifleman
				"strongZombie", // Strong Zombie
				"kingZombie", // King Zombie
				"itemZombie" }; // Item Zombie

		for (int x = 0; x < characterPrefix.length; x++) {
			characterImages.put(characterPrefix[x],
					findClassSprites(characterPrefix[x]));
			deathImages.put(characterPrefix[x],
					findStateSprites(AnimationState.Dying, characterPrefix[x]));
		}
	}

	/**
	 * Finds the images that corresponds to the specified prefix, and sorts them
	 * by animation state into lists. The state and corresponding images are
	 * added to a hashmap that is returned.
	 */
	private HashMap<AnimationState, List<Image>> findClassSprites(String prefix) {
		HashMap<AnimationState, List<Image>> output = new HashMap<AnimationState, List<Image>>();

		List<Image> stateSprites = findStateSprites(AnimationState.Attacking,
				prefix);
		if (stateSprites != null)
			output.put(AnimationState.Attacking, stateSprites);

		stateSprites = findStateSprites(AnimationState.Moving, prefix);
		if (stateSprites != null)
			output.put(AnimationState.Moving, stateSprites);

		stateSprites = findStateSprites(AnimationState.Idle, prefix);
		if (stateSprites != null)
			output.put(AnimationState.Idle, stateSprites);

		stateSprites = findStateSprites(AnimationState.Interacting, prefix);
		if (stateSprites != null)
			output.put(AnimationState.Interacting, stateSprites);

		return output;
	}

	/**
	 * Finds the images for the given state and character type. For example:
	 * 
	 * state = Moving prefix = gun
	 * 
	 * Would return a list of all images named gunWalk##.png
	 */
	private List<Image> findStateSprites(AnimationState state, String prefix) {
		ArrayList<Image> images = new ArrayList<Image>();
		int count = 1;

		while (MainDisplay.images.containsKey(prefix + state.toString() + count
				+ ".png")) {

			images.add(MainDisplay.images.get(prefix + state.toString() + count
					+ ".png"));
			count++;
		}

		// Adds the final death image to the Dying state
		if (state == AnimationState.Dying)
			images.add(MainDisplay.images.get(prefix + "Dead.png"));
		return images;
	}

	/**
	 * Updates the visible JPanel. If the gameDisplay object hasn't been
	 * initialized, initializes the panel and adds it to the JFrame.
	 */
	void startGame(JPanel change) {
		gameDisplay = (GamePanel) change;
		this.add(gameDisplay);
		menuDisplay.setVisible(false);

	}

	/**
	 * Calls the second constructor for gameDisplay that loads a saved game.
	 * Makes the game panel visible, and hides the menu panel.
	 */
	void continueGame() {
		gameDisplay = new GamePanel(this);

		if (gameDisplay.gameInitialized()) {
			this.add(gameDisplay);
			gameDisplay.setVisible(true);
			menuDisplay.setVisible(false);
		} else {
			menuDisplay.setVisible(true);
			gameDisplay.setVisible(false);
		}
	}

	/**
	 * starts map editor
	 */
	public void startMapEditor() {
		customDisplay = new CustomMapDisplay(this);
		this.setVisible(false);
		customDisplay.setVisible(true);
	}

	/**
	 * Changes the display back to the starting menu. Hides the game view.
	 */
	public void returnToMenu() {
		menuDisplay.setVisible(true);

		if (gameDisplay != null) {
			this.remove(gameDisplay);
			gameDisplay = null;
		}
	}

	/**
	 * Takes all of the files in the passed directory and loads them into the
	 * image hashmap. If there are any directories encountered, this method is
	 * recursively cast on each directory and all possible subdirectories to
	 * allow dynamic modification and reorganization of the image package.
	 */
	private void loadImages(File folder) {

		File[] files = folder.listFiles();

		if (files != null) {
			for (File f : files) {
				if (f.isDirectory())
					loadImages(f);
				else {
					// Don't load fonts
					if (!f.getName().endsWith("ttf"))
						try {
							images.put(f.getName(), ImageIO.read(f));
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			}
		}
	}

	/**
	 * Ensures that the user gets an opportunity to save the game status before
	 * exiting the program.
	 */
	private class MainWindowListener extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent e) {

			int response;
			if (gameDisplay != null) {
				if (gameDisplay.isVisible()) {
					response = JOptionPane.showConfirmDialog(null,
							"Would you like to save your game?");

					switch (response) {
					case JOptionPane.NO_OPTION:
						// Close without saving
						System.exit(0);
					case JOptionPane.CANCEL_OPTION:
						MainDisplay.this
								.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
						return;
					case JOptionPane.YES_OPTION:
						// Save the game, then exit
						gameDisplay.saveGame();
						System.exit(0);
					}
				}
			}
			else{
				System.exit(0);
			}
		}
	}

	public static void main(String[] args) {
		MainDisplay mainFrame = new MainDisplay();
		mainFrame.setVisible(true);
	}
}
