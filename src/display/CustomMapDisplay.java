package display;

import items.Armor;
import items.Optics;
import items.Overdrive;
import items.Painkiller;
import items.Stimulant;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import maps.CustomMapFactory;
import maps.EmptyTile;
import maps.HealTile;
import maps.NormalTile;
import maps.Tile;
import maps.TrapTile;
import maps.WallTile;
import characters.GameObject;
import characters.playable.Assault;
import characters.playable.Captain;
import characters.playable.Gunman;
import characters.playable.MachineGunner;
import characters.playable.Medic;
import characters.playable.PlaceHolder;
import characters.playable.Rifleman;
import characters.props.Door;
import characters.unplayable.ItemZombie;
import characters.unplayable.KingZombie;
import characters.unplayable.StrongZombie;
import characters.unplayable.Zombie;

/**
 * provides a user interface to create custom map
 */
public class CustomMapDisplay extends JFrame {
	private Tile[][] map;
	private Point selected;
	private ArrayList<Tile> tileList;
	private ArrayList<GameObject> gameObjectList;
	private MapDisplay mapPanel;
	private JTextField rowInput, colInput;
	private MainDisplay main;
	private CustomMapDisplay reference;

	/**
	 * starts up CustomMapDisplay
	 */

	public CustomMapDisplay(MainDisplay menu) {
		reference = this;
		this.setSize(800, 800);
		this.setLocation(200, 200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(2, 2));
		this.main = menu;
		initialize();
		initializeGUI();
	}

	/**
	 * displays and allows user to select from possible tiles
	 */
	private class TileDisplay extends JPanel {
		public TileDisplay() {
			Dimension d = new Dimension(MainDisplay.CELLWIDTH * 2,
					MainDisplay.CELLHEIGHT * tileList.size());
			this.setSize(d);
			this.setPreferredSize(d);
			this.addMouseListener(new TileListener());
		}

		@Override
		protected void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			super.paintComponent(g2);
			for (int i = 0; i < tileList.size(); i++) {
				Tile tile = tileList.get(i);
				g2.drawImage(MainDisplay.images.get(tile.getImageFile()),
						MainDisplay.CELLWIDTH / 2, MainDisplay.CELLHEIGHT * i,
						MainDisplay.CELLWIDTH, MainDisplay.CELLHEIGHT, null);
			}
		}

		/**
		 * Select tile to add
		 */
		private class TileListener extends MouseAdapter {
			@Override
			public void mousePressed(MouseEvent e) {// change selected tile
				if (e.getY() >= 0
						&& e.getY() < MainDisplay.CELLHEIGHT * tileList.size()
						&& e.getX() >= MainDisplay.CELLWIDTH / 2
						&& e.getX() < MainDisplay.CELLWIDTH
								+ MainDisplay.CELLWIDTH / 2) {
					Tile tile = tileList.get(e.getY() / MainDisplay.CELLHEIGHT);
					map[selected.x][selected.y] = copyTile(tile);
					mapPanel.repaint();
				}
			}
		}
	}

	/**
	 * Displays possible GameObjects and lets user select
	 */
	private class GameObjectDisplay extends JPanel {
		public GameObjectDisplay() {
			Dimension d = new Dimension(MainDisplay.CELLWIDTH
					* gameObjectList.size(), MainDisplay.CELLHEIGHT * 2);
			this.setSize(d);
			this.setPreferredSize(d);
			this.addMouseListener(new GameObjectListener());
		}

		@Override
		protected void paintComponent(Graphics g) { // paint possible
													// gameobjects
			Graphics2D g2 = (Graphics2D) g;
			super.paintComponent(g2);
			for (int i = 0; i < gameObjectList.size(); i++) {
				GameObject object = gameObjectList.get(i);
				g2.drawImage(MainDisplay.images.get(object.getImageFile()), i
						* MainDisplay.CELLWIDTH, MainDisplay.CELLHEIGHT / 2,
						MainDisplay.CELLWIDTH, MainDisplay.CELLHEIGHT, null);
			}
		}

		/**
		 * Select GameObject to add
		 */
		private class GameObjectListener extends MouseAdapter {
			@Override
			public void mousePressed(MouseEvent e) {// change selected tile
				if (e.getY() >= MainDisplay.CELLHEIGHT / 2
						&& e.getY() < MainDisplay.CELLHEIGHT
								+ MainDisplay.CELLHEIGHT / 2
						&& e.getX() >= 0
						&& e.getX() < MainDisplay.CELLWIDTH
								* gameObjectList.size()) {
					GameObject go = gameObjectList.get(e.getX()
							/ MainDisplay.CELLWIDTH);
					if (map[selected.x][selected.y].isAccessible()) {
						map[selected.x][selected.y].setOccupant(copyGameObject(
								go, selected.x, selected.y));
						mapPanel.repaint();
					}
				}
			}
		}
	}

	private class MapDisplay extends JPanel {
		public MapDisplay() {
			this.addMouseListener(new MapListener());
		}

		@Override
		protected void paintComponent(Graphics g) {
			Dimension d = new Dimension(map[0].length * MainDisplay.CELLWIDTH,
					map.length * MainDisplay.CELLHEIGHT);
			this.setSize(d);
			this.setPreferredSize(d);
			Graphics2D g2 = (Graphics2D) g;
			super.paintComponent(g2);

			// for each map tile
			for (int i = 0; i < map.length; i++) {
				for (int j = 0; j < map[i].length; j++)
					// Draw the floor tiles
					paintTiles(i, j, g2);

			}
			int col = selected.y;
			int row = selected.x;
			g2.setColor(new Color(255, 255, 0, 100));
			g2.fillRect(col * MainDisplay.CELLWIDTH, row // highlight selected
															// tile
					* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
					MainDisplay.CELLHEIGHT);
		}

		/**
		 * Used to paint the tile images. Checks to see if the tile is visible
		 * and if the tile has been seen before.
		 */
		private void paintTiles(int row, int col, Graphics2D g2) {
			Tile t = map[row][col];
			// Paint the image
			g2.drawImage(
					// paint tile
					MainDisplay.images.get(map[row][col].getImageFile()), col
							* MainDisplay.CELLWIDTH, row
							* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
					MainDisplay.CELLHEIGHT, null);
			if (t.getOccupant() != null) {// paint occupant
				g2.drawImage(MainDisplay.images.get(map[row][col].getOccupant()
						.getImageFile()), col * MainDisplay.CELLWIDTH, row
						* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
						MainDisplay.CELLHEIGHT, null);
			}
		}

		/**
		 * Select tile
		 */
		private class MapListener extends MouseAdapter {
			@Override
			public void mousePressed(MouseEvent e) {// change selected tile
				if (e.getY() >= 0
						&& e.getY() < MainDisplay.CELLHEIGHT * map.length
						&& e.getX() >= 0
						&& e.getX() < MainDisplay.CELLWIDTH * map[0].length) {
					selected = new Point(e.getY() / MainDisplay.CELLWIDTH,
							e.getX() / MainDisplay.CELLHEIGHT);
					mapPanel.repaint();
				}
			}
		}

	}

	/**
	 * Sets up the map and list of possible tiles and game objects
	 */
	private void initialize() {
		selected = new Point(0, 0);// default selected point
		map = new Tile[15][15]; // default map is 15x15 with normal tiles
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				map[i][j] = new NormalTile();
			}
		}
		// make list of possible tiles and game objects
		tileList = new ArrayList<Tile>();
		tileList.add(new NormalTile());
		WallTile wTile = new WallTile();
		wTile.changeImage("bWall.png");
		tileList.add(wTile);
		HealTile hTile = new HealTile();
		hTile.stepOnEffect(new Gunman(-1, -1));
		hTile.setOccupant(null);
		tileList.add(hTile);
		TrapTile tTile = new TrapTile();
		tTile.stepOnEffect(new Gunman(-1, -1));
		tTile.setOccupant(null);
		tileList.add(tTile);
		tileList.add(new EmptyTile());
		gameObjectList = new ArrayList<GameObject>();
		gameObjectList.add(new Assault(-1, -1));
		gameObjectList.add(new Captain(-1, -1));
		gameObjectList.add(new Gunman(-1, -1));
		gameObjectList.add(new MachineGunner(-1, -1));
		gameObjectList.add(new Medic(-1, -1));
		gameObjectList.add(new Rifleman(-1, -1));
		gameObjectList.add(new Zombie(-1, -1));
		gameObjectList.add(new ItemZombie(-1, -1));
		gameObjectList.add(new StrongZombie(-1, -1));
		gameObjectList.add(new KingZombie(-1, -1));
		gameObjectList.add(new Armor(-1, -1));
		gameObjectList.add(new Overdrive(-1, -1));
		gameObjectList.add(new Optics(-1, -1));
		gameObjectList.add(new Painkiller(-1, -1));
		gameObjectList.add(new Stimulant(-1, -1));
		gameObjectList.add(new Door(-1, -1, true));
		gameObjectList.add(new PlaceHolder(-1, -1));

	}

	/**
	 * Sets up the GUI
	 */
	private void initializeGUI() {
		mapPanel = new MapDisplay();
		JScrollPane mapPane = new JScrollPane(mapPanel);
		this.add(mapPane);
		JPanel upperRightPane = new JPanel(new GridLayout(1, 2));
		JScrollPane tilePane = new JScrollPane(new TileDisplay());
		upperRightPane.add(tilePane);
		JPanel dimensionPane = new JPanel(new GridLayout(4, 1));
		JLabel row = new JLabel("Row Dimension");
		JLabel col = new JLabel("Column Dimension");
		rowInput = new JTextField("15");
		rowInput.addActionListener(new RowListener());
		colInput = new JTextField("15");
		colInput.addActionListener(new ColListener());
		dimensionPane.add(row);
		dimensionPane.add(rowInput);
		dimensionPane.add(col);
		dimensionPane.add(colInput);
		upperRightPane.add(dimensionPane);
		this.add(upperRightPane);
		JScrollPane objectPane = new JScrollPane(new GameObjectDisplay());
		this.add(objectPane);
		JPanel buttonPanel = new JPanel(new GridLayout(2, 1));
		JButton saveButton = new JButton("Save map");
		saveButton.addActionListener(new SaveListener());
		JButton doneButton = new JButton("Done");
		doneButton.addActionListener(new DoneListener());
		buttonPanel.add(saveButton);
		buttonPanel.add(doneButton);
		this.add(buttonPanel);
	}

	/**
	 * Listens for change in row dimension
	 */
	private class RowListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				int row = Integer.parseInt(rowInput.getText());
				if (row > 6 && row < 30) {
					resetMap(row, map[0].length);
				} else {
					rowInput.setText(map.length + "");
				}
			} catch (RuntimeException e) {
				rowInput.setText(map.length + "");
			}
			mapPanel.repaint();
		}
	}

	/**
	 * Listens for change in column dimension
	 */
	private class ColListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				int col = Integer.parseInt(colInput.getText());
				if (col > 6 && col <= 40) {
					resetMap(map.length, col);
				} else {
					colInput.setText(map[0].length + "");
				}
			} catch (RuntimeException e) {
				colInput.setText(map.length + "");
			}
			mapPanel.repaint();
		}
	}

	/**
	 * Resets map when dimensions changed
	 */
	private void resetMap(int row, int col) {
		map = new Tile[row][col];
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				map[i][j] = new NormalTile();
			}
		}
	}

	/**
	 * Saves map to customMap.txt
	 */
	private class SaveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			FileWriter write = null;
			try {
				write = new FileWriter("customMap.txt");
				PrintWriter diskFile = new PrintWriter(write);
				diskFile.print(CustomMapFactory.turnToMapText(map));
				diskFile.close();
				JOptionPane.showMessageDialog(null,
						"Save successful");
			} catch (IOException ioe) {
				JOptionPane.showMessageDialog(null,
						"Error with saving file, try again");
				System.out.println("Could not create the new file: " + ioe);
			}
		}

	}

	private class DoneListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			main.setVisible(true);
			reference.setVisible(false);
		}

	}

	/**
	 * returns tile of same type
	 */
	private Tile copyTile(Tile tile) {
		if (tile instanceof NormalTile) {
			return new NormalTile();
		} else if (tile instanceof WallTile) {
			WallTile wTile = new WallTile();
			wTile.changeImage("bWall.png");
			return wTile;
		} else if (tile instanceof TrapTile) {
			TrapTile tTile = new TrapTile();
			tTile.stepOnEffect(new Gunman(-1, -1));
			tTile.setOccupant(null);
			return tTile;
		} else if (tile instanceof HealTile) {
			HealTile hTile = new HealTile();
			hTile.stepOnEffect(new Gunman(-1, -1));
			hTile.setOccupant(null);
			return hTile;
		} else {
			// default is empty
			return new EmptyTile();
		}
	}

	/**
	 * returns a GameObject of the same type
	 */
	private GameObject copyGameObject(GameObject go, int row, int col) {
		if (go instanceof Assault) {
			return new Assault(row, col);
		} else if (go instanceof Captain) {
			return new Captain(row, col);
		} else if (go instanceof Gunman) {
			return new Gunman(row, col);
		} else if (go instanceof MachineGunner) {
			return new MachineGunner(row, col);
		} else if (go instanceof Medic) {
			return new Medic(row, col);
		} else if (go instanceof Rifleman) {
			return new Rifleman(row, col);
		} else if (go instanceof Door) {
			return new Door(row, col, true);
		} else if (go instanceof Zombie) {
			return new Zombie(row, col);
		} else if (go instanceof ItemZombie) {
			return new ItemZombie(row, col);
		} else if (go instanceof KingZombie) {
			return new KingZombie(row, col);
		} else if (go instanceof StrongZombie) {
			return new StrongZombie(row, col);
		} else if (go instanceof Armor) {
			return new Armor(row, col);
		} else if (go instanceof Overdrive) {
			return new Overdrive(row, col);
		} else if (go instanceof Optics) {
			return new Optics(row, col);
		} else if (go instanceof Painkiller) {
			return new Painkiller(row, col);
		} else if (go instanceof Stimulant) {
			return new Stimulant(row, col);
		} else if (go instanceof Door) {
			return new Door(row, col, true);
		} else if (go instanceof PlaceHolder) {
			return new PlaceHolder(row, col);
		} else {
			return null;
		}
	}
}
