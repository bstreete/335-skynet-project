package display;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Timer;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import soundplayer.SoundPlayer;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel {
	private boolean gameMenu;
	private MainDisplay sourceFrame;
	private Random generate;
	private int num;

	public MenuPanel(MainDisplay source) {
		sourceFrame = source;
		this.setSize(MainDisplay.WINDOWWIDTH, MainDisplay.WINDOWHEIGHT);
		gameMenu = false;
		Timer timer = new Timer();
		SoundPlayer.playFile("./SkynetSounds/roar8.mp3");
		this.addMouseListener(new MenuListener());

		// @TODO: use this to generate random maps. Still need to be
		// implemented.
		generate = new Random();
		num = generate.nextInt(3) + 1;
		// Runs at roughly 60 fps. Repaint checks to see where the mouse is, and
		// highlights text appropriately
		timer.scheduleAtFixedRate(new ImageTimer(this),
				MainDisplay.REFRESH_RATE, MainDisplay.REFRESH_RATE);
	}

	/*
	 * Draws the splash screen and main menu
	 */
	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		Point mouse = getMousePosition();

		// Set Background to gray
		g2.setColor(MainDisplay.backgroundColor);
		g2.fillRect(0, 0, getWidth(), getHeight());

		// Draw image over black background
		if (MainDisplay.images
				.containsKey(MainDisplay.IMAGEDIR + "blueBio.png")) {
			Image background = MainDisplay.images.get(MainDisplay.IMAGEDIR
					+ "blueBio.png");
			g2.drawImage(background, 0, 0, null);
		} else {
			try {
				Image background = ImageIO.read(new File(MainDisplay.IMAGEDIR
						+ "blueBio.png"));
				g2.drawImage(background, 300, 050, null);
			} catch (IOException e) {
			}
		}

		// Menu Text
		g2.setColor(Color.ORANGE);
		g2.setFont(MainDisplay.menuFont);

		if (!gameMenu) {
			g2.drawString("Start a New Game", 10, 40);
			g2.drawString("Continue Last Game", 10, 70);
			g2.drawString("Map Editor", 10, 100);
		} else {
			g2.drawString("Elimination", 10, 40);
			g2.drawString("Assassination", 10, 70);
			g2.drawString("Survival", 10, 100);
			g2.drawString("Play custom map", 10, 130);
			g2.drawString("Main Menu", 10, 160);
		}

		// Highlight menu options when mouse hovers over them
		if (mouse != null) {
			if (mouse.x <= 425) {
				if (mouse.y <= 160) {
					g2.setColor(Color.YELLOW);
					// Return to main menu
					if (mouse.y > 130) {
						if (gameMenu) {
							g2.drawString("Main Menu", 10, 160);
						}
					} else if (mouse.y > 100) {
						if (gameMenu)
							g2.drawString("Play custom map", 10, 130);
					}

					// Survival / records
					else if (mouse.y > 70) {
						if (gameMenu)
							g2.drawString("Survival", 10, 100);
						else
							g2.drawString("Map Editor", 10, 100);
					}
					// Assassination / continue
					else if (mouse.y > 40) {
						if (gameMenu)
							g2.drawString("Assassination", 10, 70);
						else
							g2.drawString("Continue Last Game", 10, 70);
					}
					// Elimination / start new
					else if (mouse.y > 10) {
						if (gameMenu)
							g2.drawString("Elimination", 10, 40);
						else
							g2.drawString("Start a New Game", 10, 40);
					}
				}
			}
		}

	}

	/**
	 * Finds where the mouse was pressed, and determines what menu option that
	 * corresponds to.
	 */
	private class MenuListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			Point mouseLocation = e.getPoint();

			// If on the left side of the screen
			if (mouseLocation.x <= 425) {

				if (mouseLocation.y <= 160) {

					// Return to Main Menu
					if (mouseLocation.y > 130) {
						if (gameMenu) {
							SoundPlayer
									.playFile("./SkynetSounds/Geoscape - 03.mp3");
							gameMenu = false;
						}
					} else if (mouseLocation.y > 100) {
						if (gameMenu) {
							// play custom map
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							sourceFrame.startGame(new GamePanel("Level 1",
									MainDisplay.MAPDIR
											+ "eliminationDescription",
									"customMap.txt", sourceFrame));
							gameMenu = false;
						}

					}

					// Survival / records
					else if (mouseLocation.y > 70) {
						// Start new survival mode
						if (gameMenu) {
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							// JOptionPane.showMessageDialog(
							// MenuPanel.this.getRootPane(),
							// "Survival mode is still in development.");
							num = generate.nextInt(3) + 1;
							sourceFrame.startGame(new GamePanel("Survival",
									MainDisplay.MAPDIR + "survivalDescription",
									MainDisplay.MAPDIR + "survivalMap" + num,
									sourceFrame));
							gameMenu = false;
						} else {
							sourceFrame.startMapEditor();
							gameMenu = false;
						}
					}

					// Assassination / continue
					else if (mouseLocation.y > 40) {
						// start new assassination
						if (gameMenu) {
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							num = generate.nextInt(3) + 1;
							sourceFrame.startGame(new GamePanel(
									"Assassination", MainDisplay.MAPDIR
											+ "assassinationDescription",
									MainDisplay.MAPDIR + "assassinationMap"
											+ num, sourceFrame));
							gameMenu = false;
						} else { // Continue Save
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							sourceFrame.continueGame();
						}
					}

					// Elimination / start new
					else if (mouseLocation.y > 10) {

						// Start new elimination
						if (gameMenu) {
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							num = generate.nextInt(3) + 1;
							sourceFrame
									.startGame(new GamePanel("Level 1",
											MainDisplay.MAPDIR
													+ "eliminationDescription",
											MainDisplay.MAPDIR
													+ "eliminationMap" + num,
											sourceFrame));
							gameMenu = false;
						} else {
							// Change to game menu
							SoundPlayer.playFile("./SkynetSounds/BLEEP1.mp3");
							gameMenu = true;
						}
					}
				}
			}

			repaint();
		}
	}
}