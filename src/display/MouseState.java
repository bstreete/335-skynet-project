package display;

/*
 * Used to track what the mouse is currently doing. Can have new states added. 
 */
public enum MouseState {

	UsingItem, Selection, Moving, UsingAbility;
}
