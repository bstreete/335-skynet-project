package display;

import java.awt.Event;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import maps.Tile;
import model.Coordinator;
import model.Message;
import model.TurnOrder;
import characters.GameCharacter;
import characters.playable.Assault;
import characters.playable.Captain;
import characters.playable.Gunman;
import characters.playable.MachineGunner;
import characters.playable.Medic;
import characters.playable.Rifleman;

public class PartySelectionPanel extends JFrame {
	private List<GameCharacter> placeholders;
	private MainDisplay sourceFrame;
	private List<GameCharacter> goodList;
	private JList<String> selectFrom;
	private JList<String> selected;
	private PartySelectionPanel ref;
	private Tile[][] map;
	private Coordinator game;
	private GamePanel gamePanel;
	private JLabel characterInfo;

	public PartySelectionPanel(MainDisplay source,
			List<GameCharacter> placeholders, Tile[][] map, Coordinator game,
			GamePanel gamePanel) {
		ref = this;
		this.game = game;
		this.gamePanel = gamePanel;
		this.placeholders = placeholders;
		this.map = map;
		this.sourceFrame = source;
		this.goodList = game.getFriendlyList();
		this.setSize(500, 500);
		this.setLayout(new GridLayout(1, 3));
		selectFrom = new JList<String>(new UnitList());
		selectFrom.addListSelectionListener(new SelectListener());
		characterInfo=new JLabel("");
		JPanel characterPanel=new JPanel(new GridLayout(2,1));
		characterPanel.add(selectFrom);
		characterPanel.add(characterInfo);
		this.add(characterPanel);
		JPanel buttonPanel = new JPanel(new GridLayout(3, 1));
		JButton addButton = new JButton("Add");
		JButton removeButton = new JButton("Remove");
		JButton startButton = new JButton("Start Game");
		addButton.addActionListener(new AddListener());
		removeButton.addActionListener(new RemoveListener());
		startButton.addActionListener(new StartListener());
		buttonPanel.add(addButton);
		buttonPanel.add(removeButton);
		buttonPanel.add(startButton);
		this.add(buttonPanel);
		JPanel selectPanel = new JPanel(new GridLayout(2, 1));
		JLabel selectLabel = new JLabel("Select " + placeholders.size()
				+ " units");
		selected = new JList<String>(new SelectedList());
		selectPanel.add(selectLabel);
		selectPanel.add(selected);
		this.add(selectPanel);

		this.addWindowListener(new PartyWindow());
	}
	private class AddListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (selectFrom.getSelectedValue() != null) {
				if (selected.getModel().getSize() >= placeholders.size()) {
					JOptionPane.showMessageDialog(null,
							"Too many characters selected.");
				} else {
					((SelectedList) selected.getModel()).add(selectFrom
							.getSelectedValue());

				}
			}
			selected.updateUI();
		}
	}
	private class SelectListener implements ListSelectionListener{
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(selectFrom.getSelectedValue()==null ||selectFrom.getSelectedValue().equals("")){
			characterInfo.setText("");
			}
			else{
				GameCharacter c= getNewCharacter(selectFrom.getSelectedValue(),-1,-1);
				characterInfo.setText(c.getDescription());
			}
		}	
	}
	private class RemoveListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (selected.getSelectedIndex() < 0
					|| selected.getSelectedIndex() >= selected.getModel()
							.getSize()) {

			} else {
				((SelectedList) selected.getModel()).remove(selected
						.getSelectedIndex());
			}
			selected.updateUI();

		}
	}
	
	private class StartListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (selected.getModel().getSize() == placeholders.size()) {
				for (GameCharacter place : placeholders) {
					String addMe = selected.getModel().getElementAt(0);
					((SelectedList) selected.getModel()).remove(0);
					GameCharacter newCharacter = getNewCharacter(addMe,
							place.getRow(), place.getCol());
					map[place.getRow()][place.getCol()]
							.setOccupant(newCharacter);
					map[place.getRow()][place.getCol()].setAccessible(false);
					goodList.add(newCharacter);
					game.getCreatureList().add(newCharacter);
					game.getLivingList().add(newCharacter);
					game.getTurnOrder().add(newCharacter);
					newCharacter.setAnimationMap(MainDisplay.characterImages
							.get(newCharacter.getImagePrefix()));
				}
				ref.setVisible(false);
				sourceFrame.setVisible(true);
				TurnOrder t = new TurnOrder(game.getLivingList());
				game.setTurnOrder(t);
				game.updateVisibility();
				gamePanel.update(Message.Start);
				game.nextTurn(game.getCurrentTurn());
			} else {
				JOptionPane.showMessageDialog(null,
						"Not enough characters selected.");
			}

		}
	}

	private GameCharacter getNewCharacter(String name, int row, int col) {
		switch (name) {
		case "Assault":
			return new Assault(row, col);
		case "Medic":
			return new Medic(row, col);
		case "Captain":
			return new Captain(row, col);
		case "Rifleman":
			return new Rifleman(row, col);
		case "Gunman":
			return new Gunman(row, col);
		case "MachineGunner":
			return new MachineGunner(row, col);
		default:
			return null;
		}
	}

	private class UnitList<String> implements ListModel<String> {
		private List<String> unitnames;

		public UnitList() {
			unitnames = new ArrayList<String>();
			unitnames.add((String) "Assault");
			unitnames.add((String) "Rifleman");
			unitnames.add((String) "Medic");
			unitnames.add((String) "Captain");
			unitnames.add((String) "MachineGunner");
			unitnames.add((String) "Gunman");
		}

		@Override
		public void addListDataListener(ListDataListener arg0) {
		}

		@Override
		public String getElementAt(int arg) {
			if (arg < 0 || arg >= unitnames.size())
				return (String) "";
			else
				return unitnames.get(arg);
		}

		@Override
		public int getSize() {

			return unitnames.size();
		}

		@Override
		public void removeListDataListener(ListDataListener arg0) {

		}

	}

	private class SelectedList<String> implements ListModel<String> {
		ArrayList<String> selected;

		public SelectedList() {
			selected = new ArrayList<String>();
		}

		@Override
		public void addListDataListener(ListDataListener l) {

		}

		public void add(String name) {
			selected.add(name);
		}

		public void remove(int i) {
			selected.remove(i);
		}

		@Override
		public String getElementAt(int index) {
			return selected.get(index);
		}

		@Override
		public int getSize() {
			return selected.size();
		}

		@Override
		public void removeListDataListener(ListDataListener l) {

		}

	}

	private class PartyWindow extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

}
