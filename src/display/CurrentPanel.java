package display;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import characters.GameCharacter;
import characters.playable.MachineGunner;

/**
 * Displays the information for the character with the active turn. Displays its
 * name, health, range, mobility, and its image.
 */
@SuppressWarnings("serial")
public class CurrentPanel extends JPanel {

	GameCharacter currentCharacter;
	GamePanel container;
	private JLabel image, name, health, range, mobility;

	public CurrentPanel(GameCharacter current, GamePanel source) {

		container = source;
		
		this.setBackground(MainDisplay.backgroundColor);
		this.setOpaque(true);
		
		name = new JLabel();
		image = new JLabel();
		health = new JLabel();
		range = new JLabel();
		mobility = new JLabel();

		name.setFont(MainDisplay.selectedFont);
		name.setForeground(Color.orange);
		name.setBackground(MainDisplay.backgroundColor);
		name.setOpaque(true);
		
		image.setFont(MainDisplay.selectedFont);
		image.setForeground(Color.orange);
		image.setBackground(MainDisplay.backgroundColor);
		image.setOpaque(true);
		
		health.setFont(MainDisplay.selectedFont);
		health.setForeground(Color.orange);
		health.setBackground(MainDisplay.backgroundColor);
		health.setOpaque(true);
		
		range.setFont(MainDisplay.selectedFont);
		range.setForeground(Color.orange);
		range.setBackground(MainDisplay.backgroundColor);
		range.setOpaque(true);
		
		mobility.setFont(MainDisplay.selectedFont);
		mobility.setForeground(Color.orange);
		mobility.setBackground(MainDisplay.backgroundColor);
		mobility.setOpaque(true);
		
		JPanel stats = new JPanel();
		stats.setLayout(new GridLayout(4, 1));
		JPanel imagePanel = new JPanel();
		imagePanel.setLayout(new GridLayout(2, 1));

		JLabel label = new JLabel("Current Turn");
		label.setFont(MainDisplay.selectedFont);
		label.setForeground(Color.orange);
		label.setBackground(MainDisplay.backgroundColor);
		label.setOpaque(true);
		
		imagePanel.add(label);
		imagePanel.add(image);

		stats.add(name);
		stats.add(health);
		stats.add(range);
		stats.add(mobility);

		this.setLayout(new GridLayout(1, 2));

		this.add(imagePanel);
		this.add(stats);

		updateCharacter(current);
		this.addMouseListener(new CurrentListener());
	}

	/*
	 * Changes the current turn to a new character. Updates the text and images.
	 */
	public void updateCharacter(GameCharacter current) {

		if (current != null) {
			String imagePath = current.getImageFile();

			currentCharacter = current;
			if (imagePath == null)
				image.setIcon(null);
			else {
				if (currentCharacter instanceof MachineGunner)
					image.setIcon(new ImageIcon(MainDisplay.images.get(
							imagePath).getScaledInstance(90, 45,
							Image.SCALE_SMOOTH)));

				else
					image.setIcon(new ImageIcon(MainDisplay.images.get(
							imagePath).getScaledInstance(50, 50,
							Image.SCALE_SMOOTH)));
			}
			name.setText("Name: " + currentCharacter.getName());
			health.setText("Health: " + currentCharacter.getHealth() + "");
			range.setText("Range: " + currentCharacter.getRange() + "");
			mobility.setText("Mobility: " + currentCharacter.getMobility() + "");
		}
	}

	/**
	 * Handles mouse clicks on the current turn panel that displays the active
	 * characters stats and image. If clicked, reselects the currently active
	 * character.
	 */
	private class CurrentListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			Point mouseLocation = getMousePosition();

					container.setObjectSelected(currentCharacter);

		}
	}
}
