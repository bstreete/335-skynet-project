package display;

import items.GameItem;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import maps.Tile;
import model.Coordinator;
import model.Message;
import model.Observer;
import soundplayer.SoundPlayer;
import characters.AnimationState;
import characters.GameCharacter;
import characters.GameObject;
import characters.playable.MachineGunner;
import characters.playable.PlayableCharacter;
import characters.props.Door;
import characters.unplayable.UnplayableCharacter;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Observer {

	// Variables that track game state
	private MouseState state;
	int horizOffset, vertOffset;
	GameObject selected;
	private Coordinator game;
	Point destination;

	// Graphical elements
	private JTable itemList;
	private ItemTableModel itemModel;
	private CurrentPanel currentPanel;
	private MapPanel mapPanel;
	private SelectionPanel selectionPanel;
	private CommandPanel commandPanel;
	private JPanel objectPanel;
	private JLabel messages;
	private MainDisplay sourceFrame;
	private TurnPanel turnPanel;
	private static SoundPlayer bgm;
	private volatile boolean doneAnimating;

	/**
	 * Constructor for starting a new game. Passes the strings to a new instance
	 * of coordinator which reads from the map string.
	 * 
	 * After the game is initialized, initializes all of the graphical elements,
	 * and prepares the gui for user input.
	 */
	public GamePanel(String name, String description, String map,
			MainDisplay source) {
		sourceFrame = source;
		game = new Coordinator(name, description, map);
		game.addObserver(this);
		doneAnimating = true;
		assignImages();
		initializePanels();
		sourceFrame.setVisible(false);
		PartySelectionPanel selectPanel = new PartySelectionPanel(source,
				game.getPlaceholders(), game.getMap(), game, this);
		selectPanel.setVisible(true);

	}

	/**
	 * Constructor that is used when loading the previously saved game. If the
	 * file cannot be read from, the game returns to the main menu with a
	 * message saying there isn't a save to load.
	 */
	public GamePanel(MainDisplay source) {
		sourceFrame = source;

		// If the game doesn't load
		if (!loadSave()) {
			JOptionPane.showMessageDialog(sourceFrame,
					"No saved game detected.");
			sourceFrame.returnToMenu();
		} else {
			game.addObserver(this);

			initializePanels();
			updatePanels();
		}
	}

	private void assignImages() {
		for (GameCharacter c : game.getCreatureList()) {
			c.setAnimationMap(MainDisplay.characterImages.get(c
					.getImagePrefix()));
		}
	}

	/**
	 * Attempts to load a save from the file lastSave.txt. If it fails for any
	 * reason, exits with false to tell the view to return to the menu. If it
	 * loads successfully, blanks the old file out.
	 */
	private boolean loadSave() {

		try {
			FileInputStream readFile = new FileInputStream(MainDisplay.BASEDIR
					+ "lastSave.txt");
			ObjectInputStream readObject = new ObjectInputStream(readFile);

			game = (Coordinator) readObject.readObject();

			readObject.close();

			// Empties contents of file
			PrintWriter eraser = new PrintWriter(MainDisplay.BASEDIR
					+ "lastSave.txt");
			eraser.close();
			doneAnimating = true;
			bgm = new SoundPlayer("./SkynetSongs/MainMenuMusic.mp3", true);

			return true;
		} catch (Exception e) {
		}

		return false;
	}

	/**
	 * Initializes all of the panels used in the game panel. Also sets the
	 * appropriate attributes for each panel before adding them to the game
	 * panel.
	 */
	private void initializePanels() {

		initializeTable();
		JOptionPane.showMessageDialog(this, game.getDescription());

		this.state = MouseState.Selection;
		this.setSize(MainDisplay.WINDOWWIDTH, MainDisplay.WINDOWHEIGHT);
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));

		mapPanel = new MapPanel(game.getMap());

		turnPanel = new TurnPanel(game.getTurnModel());
		turnPanel.addListener(new TurnListener());
		turnPanel.setPreferredSize(new Dimension(750, 150));

		currentPanel = new CurrentPanel(game.getCurrentTurn(), this);
		currentPanel.setPreferredSize(new Dimension(750, 100));

		JPanel map = new JPanel();
		map.setLayout(new GridBagLayout());
		map.add(mapPanel);
		map.setBackground(MainDisplay.backgroundColor);

		JScrollPane pane = new JScrollPane(map);
		pane.setPreferredSize(new Dimension(750, 500));

		left.add(pane);
		left.add(currentPanel);
		left.add(turnPanel);

		this.add(left);
		selected = game.getCurrentTurn();
		((GameCharacter) selected).setAnimationState(AnimationState.Moving);

		selectionPanel = new SelectionPanel(selected);
		commandPanel = new CommandPanel();

		messages = new JLabel("");
		messages.setForeground(Color.orange);
		messages.setFont(MainDisplay.selectedFont);
		messages.setBackground(MainDisplay.backgroundColor);
		messages.setOpaque(true);

		objectPanel = new JPanel();
		objectPanel.setLayout(new GridLayout(4, 1));
		objectPanel.setPreferredSize(new Dimension(350, 800));
		objectPanel.add(selectionPanel);
		objectPanel.add(commandPanel);
		objectPanel.add(itemList);
		objectPanel.add(messages);
		this.add(objectPanel);
		destination = new Point();
	}

	/**
	 * Initializes the table that displays items and sets the appropriate
	 * information for it.
	 */
	private void initializeTable() {
		itemModel = new ItemTableModel();
		itemList = new JTable(itemModel);

		itemList.setSize(new Dimension(150, 100));
		itemList.setDefaultRenderer(Object.class, new ItemCellRenderer());
		itemList.setBackground(MainDisplay.backgroundColor);
		// Sorts items alphabetically
		itemList.setRowSorter(new TableRowSorter<ItemTableModel>(itemModel));
		// itemList.set
	}

	/**
	 * Updates the text displayed on the right side of the game.
	 */
	private void updatePanels() {

		if (selected != null) {

			selectionPanel.updateSelected(selected);
			currentPanel.updateCharacter(game.getCurrentTurn());
		}

		if (selected instanceof GameCharacter)
			commandPanel.updateCharacter(selected == game.getCurrentTurn());
		else
			commandPanel.clearPanel();
		itemList.repaint();
	}

	/**
	 * Used to handle user input via clicks. The tile clicked on is calculated
	 * by the mouse listener. The mouse state determines what kind of
	 * information is passed to the coordinator.
	 */
	private void selectTile(int row, int col) {

		switch (state) {
		case Selection:
			setObjectSelected(game.getOccupant(row, col));
			break;

		case Moving:
			game.moveCharacter((GameCharacter) selected, new Point(row, col));
			state = MouseState.Selection;
			break;

		case UsingAbility:
			game.performAction((GameCharacter) selected, new Point(row, col));
			state = MouseState.Selection;
			break;

		case UsingItem:
			selected = null;
			state = MouseState.Selection;
			break;
		}

		if (selected instanceof GameCharacter)
			commandPanel.updateCharacter(selected == game.getCurrentTurn());
		else
			commandPanel.clearPanel();
		itemList.repaint();
	}

	/**
	 * Updates the selected variable using the object passed as an argument.
	 */
	public void setObjectSelected(GameObject c) {

		if (selected instanceof GameCharacter)
			// Set previously selected item to idle
			((GameCharacter) selected).setAnimationState(AnimationState.Idle);

		// Update selected
		selected = c;

		if (c != null) {
			if (c instanceof GameCharacter)
				((GameCharacter) c).setAnimationState(AnimationState.Moving);
		}

		updatePanels();
	}

	/**
	 * Saves the current game status to the file src/lastSave.txt.
	 */
	public void saveGame() {

		FileOutputStream writeFile;
		try {
			writeFile = new FileOutputStream(MainDisplay.BASEDIR
					+ "lastSave.txt");
			ObjectOutputStream writeObject = new ObjectOutputStream(writeFile);

			writeObject.writeObject(game);
			writeObject.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to verify the game has loaded successfully. Called by the main
	 * display after sending a loadSave message.
	 * 
	 * @return
	 */
	public boolean gameInitialized() {
		if (game == null)
			return false;
		else {

			// Reload the images for each character
			for (GameCharacter c : game.getCreatureList())
				c.setAnimationMap(MainDisplay.characterImages.get(c
						.getImagePrefix()));

			return true;
		}
	}

	public synchronized void stopMusic() {
		bgm.stopPlaying();
	}

	/**
	 * Used to pass messages from the model to the display. The Message enum has
	 * a handful of different states that pass information to the user via its
	 * overridden toString method. If the message is for GameLost or GameWon, it
	 * changes the display back to the main menu. If the message is for a
	 * successful move or attack, will animate the motion before updating the
	 * message panel.
	 */
	@Override
	public void update(Message update) {

		Timer t = new Timer();

		switch (update) {
		case Start:
			game.updateCreatureList();
			updatePanels();
			turnPanel.updateJList(game.getTurnModel());

			bgm = new SoundPlayer("./SkynetSongs/MainMenuMusic.mp3", true);
			break;

		case GameLost:
			JOptionPane.showMessageDialog(this, update.toString());
			bgm.stopPlaying();
			sourceFrame.returnToMenu();
			break;
		case GameWon:
			JOptionPane.showMessageDialog(this, update.toString());
			bgm.stopPlaying();
			sourceFrame.returnToMenu();
			break;

		case ActionSuccessful:
		case TargetDestroyed:
			((GameCharacter) selected)
					.setAnimationState(AnimationState.Attacking);

			t.schedule(new ActionTimer(update), 500);
			break;

		case Moving:
			setDoneAnimating(false);
			((GameCharacter) selected).setAnimationState(AnimationState.Moving);
			animateWalk(update);
			break;

		default:
			setDoneAnimating(true);
			updateDisplay(update);
		}
	}

	/**
	 * Handles moving characters a single tile at a time. Calculates the offsets
	 * needed to make the character appear to gradually move to the tile by
	 * comparing the current tile to the next tile. Also sets the boolean
	 * leftFacing flag that is used to determine which direction a character
	 * faces while animating.
	 */
	private void animateWalk(Message update) {
		Point nextTile = game.getNextStep();

		if (nextTile != null) {
			Point current = new Point(((GameCharacter) selected).getRow(),
					((GameCharacter) selected).getCol());

			int xDiff = nextTile.y - current.y;
			int yDiff = nextTile.x - current.x;

			if (xDiff < 0) {
				horizOffset = -5;
				((GameCharacter) selected).setLeftFacing(true);
			} else if (xDiff > 0) {
				horizOffset = 5;
				((GameCharacter) selected).setLeftFacing(false);
			} else
				horizOffset = 0;

			if (yDiff < 0)
				vertOffset = -5;
			else if (yDiff > 0)
				vertOffset = 5;
			else
				vertOffset = 0;

			if (horizOffset != 0 || vertOffset != 0)
				mapPanel.resetOffsetCounter();

		}

		else
			updateDisplay(update);
	}

	/**
	 * Called after a brief wait after an update message is received. Updates
	 * the mouse state, the animation state of the current character, and
	 * refreshes the information panels.
	 * 
	 */
	private void updateDisplay(Message update) {

		state = MouseState.Selection;

		if (selected instanceof GameCharacter)
			((GameCharacter) selected).setAnimationState(AnimationState.Idle);

		selected = game.getCurrentTurn();
		updatePanels();
		messages.setText(update.toString());
	}

	/**
	 * Called by the update method. Used to display the character move/attack
	 * animations before reverting to idle status.
	 */
	private class ActionTimer extends TimerTask {

		private Message update;

		public ActionTimer(Message update) {
			this.update = update;
		}

		@Override
		public void run() {
			updateDisplay(update);
		}
	}

	/**
	 * MapPanel is responsible for painting the game board and all of the
	 * occupants inside. Does not display statistics of any kind to the player.
	 */
	private class MapPanel extends JPanel {
		private Tile[][] map;
		private int animationCounter;
		private int offsetCounter;

		/**
		 * Initializes the MapPanel with a 2d array of tiles. Sets the
		 * dimensions of the panel, then starts a scheduled timer task that
		 * calls repaint roughly 60 times per second.
		 */
		public MapPanel(final Tile[][] map) {
			Timer timer = new Timer();
			this.map = map;
			this.setPreferredSize(new Dimension(MainDisplay.CELLWIDTH
					* map[0].length, MainDisplay.CELLHEIGHT * map.length));
			this.setSize(new Dimension(MainDisplay.CELLWIDTH * map[0].length,
					MainDisplay.CELLHEIGHT * map.length));
			this.addMouseListener(new SelectListener());
			animationCounter = 1;
			offsetCounter = 1;
			// Calls repaint at roughly 60 fps.
			timer.scheduleAtFixedRate(new ImageTimer(this),
					MainDisplay.REFRESH_RATE, MainDisplay.REFRESH_RATE);
		}

		public void updateCounters() {
			animationCounter++;
			if (animationCounter % 2 == 0) {
				offsetCounter++;

				if (offsetCounter > 6) {
					horizOffset = 0;
					vertOffset = 0;
				}

				for (GameCharacter c : game.getCreatureList())
					c.incrementAnimationCounter();
			}
		}

		/**
		 * Paints the map tiles, then the creatures, then highlights the tile
		 * the mouse is currently hovering over.
		 * 
		 * Tile and creature visibility is handled by the respective paint
		 * helper methods: paintTiles, paintCharacters. The paintCharacters
		 * method also handles highlighting the currently active character.
		 */
		@Override
		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			super.paintComponent(g2);

			updateCounters();

			// for each map tile
			for (int i = 0; i < map.length; i++) {
				for (int j = 0; j < map[i].length; j++)
					// Draw the floor tiles
					paintTiles(i, j, g2);

			}

			// Paint the dead first, followed by living creatures then highlight
			paintOccupants(g2, game.getCorpseList());
			paintOccupants(g2, game.getLivingList());
			paintHighlight(g2);
		}

		/**
		 * Used to paint the tile images. Checks to see if the tile is visible
		 * and if the tile has been seen before.
		 */
		private void paintTiles(int row, int col, Graphics2D g2) {
			Tile t = map[row][col];

			// If the tile has been seen at least once
			if (t.getSeenByPlayer()) {

				// Paint the image
				g2.drawImage(
						MainDisplay.images.get(map[row][col].getImageFile()),
						col * MainDisplay.CELLWIDTH, row
								* MainDisplay.CELLHEIGHT,
						MainDisplay.CELLWIDTH, MainDisplay.CELLHEIGHT, null);

				// If tile is hidden, paint it gray
				if (t.getHiddenByFog()) {
					// Paint Dark Gray over the image
					g2.setColor(new Color(68, 68, 68, 100));
					g2.fillRect(col * MainDisplay.CELLWIDTH, row
							* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
							MainDisplay.CELLHEIGHT);
				}

				// Not hidden, paint items occupying the tile
				else {
					// Draw image of item
					if (t.getOccupant() instanceof GameItem
							|| t.getOccupant() instanceof Door)
						g2.drawImage(MainDisplay.images.get(map[row][col]
								.getOccupant().getImageFile()), col
								* MainDisplay.CELLWIDTH, row
								* MainDisplay.CELLHEIGHT,
								MainDisplay.CELLWIDTH, MainDisplay.CELLHEIGHT,
								null);
				}
			}

			// Hasn't been seen yet
			else {

				// Paint a black square instead of tiles
				g2.setColor(Color.BLACK);
				g2.fillRect(col * MainDisplay.CELLWIDTH, row
						* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
						MainDisplay.CELLHEIGHT);
			}

			// If the currently selected character can target the tile,
			// highlight it blue
			if (state == MouseState.UsingAbility || state == MouseState.Moving)
				if (t.getInRange()) {
					g2.setColor(new Color(0, 0, 255, 100));
					g2.fillRect(col * MainDisplay.CELLWIDTH, row
							* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
							MainDisplay.CELLHEIGHT);
					g2.setColor(new Color(0, 255, 255, 100));
					g2.drawRect(col * MainDisplay.CELLWIDTH, row
							* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
							MainDisplay.CELLHEIGHT);
				}
		}

		/**
		 * Used to paint character images. Checks to see if the tile is visible
		 * and if the tile has been seen before.
		 * 
		 * Highlights the active character in red if it is visible.
		 */
		private void paintOccupants(Graphics2D g2,
				List<GameCharacter> creatureList) {

			for (GameCharacter c : creatureList) {
				int row = c.getRow();
				int col = c.getCol();

				// If player character or visible opponent
				if (c instanceof Door) {

				} else if (c instanceof PlayableCharacter
						|| !map[row][col].getHiddenByFog()) {

					// If the current character is the selected character and
					// moving, paint an offset image
					if (c == selected
							&& c.getAnimationState() == AnimationState.Moving)
						paintCharacter(g2, c.getCurrentAnimation(), col, row,
								horizOffset * offsetCounter, vertOffset
										* offsetCounter, false,
								c.getLeftFacing());

					// Paint wide image
					else if ((c.getState() == AnimationState.Attacking || c instanceof MachineGunner)
							&& !(c instanceof UnplayableCharacter))
						paintCharacter(g2, c.getCurrentAnimation(), col, row,
								0, 0, true, c.getLeftFacing());

					// Paint normal width image
					else
						paintCharacter(g2, c.getCurrentAnimation(), col, row,
								0, 0, false, c.getLeftFacing());

					// Highlights the creature if it is currently selected
					if (c == selected) {
						if (selected != null) {
							g2.setColor(new Color(255, 0, 0, 50));
							g2.fillRect(col * MainDisplay.CELLWIDTH, row
									* MainDisplay.CELLHEIGHT,
									MainDisplay.CELLWIDTH,
									MainDisplay.CELLHEIGHT);
						}
					}
				}
			}
		}

		/**
		 * Helper method that paints a 32x32 object at the given coordinate
		 * tile. Used to paint all creatures. If the passed horizontal offset is
		 * positive, the image is reversed to make the character face to the
		 * right.
		 */
		private void paintCharacter(Graphics2D g2, Image currentFrame, int col,
				int row, int horizOff, int vertOff, boolean isWide,
				boolean leftFacing) {

			int startingX = (col * MainDisplay.CELLWIDTH) + horizOff;
			int startingY = (row * MainDisplay.CELLHEIGHT) + vertOff;
			int width = MainDisplay.CELLWIDTH;
			int height = MainDisplay.CELLHEIGHT;

			// If the character is moving to the right
			if (!leftFacing) {
				// Move the starting draw position to the right by one tile
				startingX = startingX + MainDisplay.CELLWIDTH;
				// Draw the character facing the opposite direction
				width = width * -1;
			}

			// If painting a two tile by one tile image, move starting x value
			// to left, increase width
			if (isWide) {
				startingX -= width;
				width += width;
			}

			g2.drawImage(currentFrame, startingX, startingY, width, height,
					null);
		}

		/**
		 * Highlights the tile the mouse is hovering over, if the mouse is on
		 * the map panel.
		 */
		private void paintHighlight(Graphics2D g2) {

			Point mouse = getMousePosition();
			// If the mouse is over the map panel
			if (mouse != null) {

				// Calculate the correct grid coordinates to paint at
				int col = mouse.x / MainDisplay.CELLWIDTH;
				int row = mouse.y / MainDisplay.CELLHEIGHT;

				g2.setColor(new Color(255, 255, 0, 100));

				g2.fillRect(col * MainDisplay.CELLWIDTH, row
						* MainDisplay.CELLHEIGHT, MainDisplay.CELLWIDTH,
						MainDisplay.CELLHEIGHT);
			}
		}

		/**
		 * Handles mouse pressed events on the map panel so that the game can
		 * process requests. Calls the selectTile method from the game panel
		 * with the coordinates clicked on by the user.
		 */
		private class SelectListener extends MouseAdapter {

			@Override
			public void mousePressed(MouseEvent e) {// select tile
				// If the current active character is player owned, allow
				// selections
				if (doneAnimating) {
					if (game.getCurrentTurn() instanceof PlayableCharacter) {
						if (e.getY() >= 0
								&& e.getY() < MainDisplay.CELLHEIGHT
										* map.length
								&& e.getX() >= 0
								&& e.getX() < MainDisplay.CELLWIDTH
										* map[0].length) {
							selectTile(e.getY() / MainDisplay.CELLHEIGHT,
									e.getX() / MainDisplay.CELLWIDTH);
							updatePanels();

							destination = new Point(e.getY()
									/ MainDisplay.CELLHEIGHT, e.getX()
									/ MainDisplay.CELLWIDTH);
						}
					}
				}
			}
		}

		private void resetOffsetCounter() {
			offsetCounter = 1;
		}
	}

	/**
	 * TurnListener is responsible for updating the currently selected character
	 * when the user clicks on the turn order. Takes the selected character and
	 * displays its stats on the right hand side of the screen.
	 */
	private class TurnListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent selection) {

			@SuppressWarnings("unchecked")
			int index = ((JList<GameCharacter>) selection.getSource())
					.getSelectedIndex();

			if (index >= 0)
				setObjectSelected(game.getTurnModel().getElementAt(index));

			updatePanels();
		}
	}

	/**
	 * Handles formatting and displaying the appropriate text for the
	 * character's inventory.
	 */
	private class ItemCellRenderer extends JLabel implements TableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable sourceTable,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int col) {

			this.setFont(MainDisplay.selectedFont);

			if (isSelected)
				this.setForeground(Color.red);
			else
				this.setForeground(Color.orange);

			GameItem item = (GameItem) sourceTable.getModel()
					.getValueAt(row, 0);
			ItemCellRenderer.this.setHorizontalAlignment(CENTER);
			setText(item.getName() + ": " + item.getDescription());

			return this;
		}
	}

	private class CommandPanel extends JPanel {

		private final int UPPER_TEXT = 30;
		private final int LOWER_TEXT = 80;
		boolean isActive;

		public CommandPanel() {

			this.setPreferredSize(new Dimension(200, 90));
			isActive = false;
			this.addMouseListener(new CommandListener());
			this.setLayout(null);

			Timer timer = new Timer();
			// Calls repaint at roughly 60 fps.
			timer.scheduleAtFixedRate(new ImageTimer(this),
					MainDisplay.REFRESH_RATE, MainDisplay.REFRESH_RATE);
		}

		public void updateCharacter(boolean active) {
			isActive = active;

		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;

			// Background
			g2.setColor(MainDisplay.backgroundColor);
			g2.fillRect(0, 0, getWidth(), getHeight());

			// Draw the basic options
			g2.setColor(Color.orange);
			g2.setFont(MainDisplay.selectedFont);

			if (selected != null) {
				if (selected instanceof GameCharacter) {
					boolean playerControlled = ((GameCharacter) selected) instanceof PlayableCharacter;
					switch (state) {
					case Selection:

						g2.drawString("Move", 40, UPPER_TEXT);

						g2.drawString(((GameCharacter) selected).getAbility()
								.getName(), 160, UPPER_TEXT);
						if (isActive && playerControlled) {
							if (((GameCharacter) selected).getItems().size() > 0)
								g2.drawString("Use Item", 40, LOWER_TEXT);

							g2.drawString("End Turn", 160, LOWER_TEXT);
						}
						break;

					case Moving:

						g2.drawString("Choose a destination", 40, UPPER_TEXT);

						g2.drawString("Cancel Move", 40, LOWER_TEXT);
						break;

					case UsingAbility:

						g2.drawString("Choose a target", 40, UPPER_TEXT);

						g2.drawString("Cancel "
								+ ((GameCharacter) selected).getAbility()
										.getName(), 40, LOWER_TEXT);

						break;
					case UsingItem:

						g2.drawString("Use Item", 120, UPPER_TEXT);
						g2.drawString("Cancel", 120, LOWER_TEXT);
						break;
					}

					// Highlight Moused Over Text
					Point mouse = getMousePosition();
					g2.setColor(Color.red);

					if (mouse != null) {
						// Not above or to the left of panel
						if (mouse.x > 0 && mouse.y > 0) {

							// Inside the bottom right location of text
							if (mouse.x < 300 && mouse.y < 90) {

								// Top Row
								if (mouse.y < 45) { // Top Left option
									if (mouse.x < 150
											&& state == MouseState.Selection) {
										g2.drawString("Move", 40, UPPER_TEXT);
									}

									// Top Right option
									else if (mouse.x > 150
											&& state == MouseState.Selection) {
										g2.drawString(
												((GameCharacter) selected)
														.getAbility().getName(),
												160, UPPER_TEXT);
									}

									else if (state == MouseState.UsingItem)
										g2.drawString("Use Item", 120,
												UPPER_TEXT);
								}

								// Bottom Row
								else {

									if (state == MouseState.UsingAbility)
										g2.drawString(
												"Cancel "
														+ ((GameCharacter) selected)
																.getAbility()
																.getName(), 40,
												LOWER_TEXT);
									else if (state == MouseState.Moving)
										g2.drawString("Cancel Move", 40,
												LOWER_TEXT);

									else if (state == MouseState.UsingItem)
										g2.drawString("Cancel", 120, LOWER_TEXT);
									// Bottom Left Option
									else if (((GameCharacter) selected)
											.getItems().size() > 0
											&& state == MouseState.Selection
											&& mouse.x < 150
											&& playerControlled)
										g2.drawString("Use Item", 40,
												LOWER_TEXT);
									// Bottom Right Option
									else if (mouse.x > 150 && isActive
											&& playerControlled) {
										g2.drawString("End Turn", 160,
												LOWER_TEXT);
									}
								}
							}
						}
					}
				}
			}

		}

		public void clearPanel() {
			selected = null;
		}

		private class CommandListener extends MouseAdapter {

			@Override
			public void mousePressed(MouseEvent e) {
				Point location = getMousePosition();

				if (game.getCurrentTurn() instanceof PlayableCharacter
						&& selected instanceof GameCharacter) {
					// Not above or to the left of panel
					if (doneAnimating) {
						if (location.x > 0 && location.y > 0) {

							// Inside the bottom right location of text
							if (location.x < 300 && location.y < 90) {

								switch (state) {
								case Selection:
									// Top Row
									if (location.y < 45) {
										// Top Left option - Move
										if (location.x < 150) {
											state = MouseState.Moving;
											game.updateRange(
													(GameCharacter) selected,
													false);
										}
										// Top Right option - Use Ability
										else if (location.x > 150) {
											state = MouseState.UsingAbility;
											game.updateRange(
													(GameCharacter) selected,
													true);
										}
									}

									// Bottom Row
									else {
										// Bottom Left option - Use Item
										if (((GameCharacter) selected)
												.getItems().size() > 0
												&& location.x < 150) {

											state = MouseState.UsingItem;
											// Update the item list
											itemModel
													.updateItems(((GameCharacter) selected)
															.getItems());
										}

										// Bottom Right Option - End Turn
										else if (location.x > 150) {
											game.nextTurn((GameCharacter) selected);
										}
									}
									break; // End of Selection

								case UsingItem:
									// Top Row - Use Item
									if (location.y < 45) {
										GameItem item = itemModel.getValueAt(
												itemList.getSelectedRow(), 1);
										if (item != null) {
											item.useOn((GameCharacter) selected);
											((GameCharacter) selected)
													.getItems().remove(item);
											state = MouseState.Selection;
										}
									}

									// Bottom Row - Cancel
									else {
										state = MouseState.Selection;
										itemModel.clear();
										itemList.clearSelection();
									}
									break; // End of Using Item

								case Moving:
								case UsingAbility:
									// Bottom Row - Cancel
									if (location.y > 45)
										state = MouseState.Selection;

								} // End of switch(state)
								updatePanels();
							}
						} // End of if statements
					}
				}
			}
		} // End of Command Listener
	}

	public void setDoneAnimating(boolean b) {
		doneAnimating = b;
	}
}