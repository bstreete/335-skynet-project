package display;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionListener;

import characters.GameCharacter;
import characters.playable.MachineGunner;
import characters.playable.PlayableCharacter;

/**
 * Displays the current turn order. Will eventually show the next five turns at
 * a time with the images displayed. Once the image is clicked, the game will
 * select the corresponding creature.
 */
@SuppressWarnings("serial")
public class TurnPanel extends JPanel {

	private Color blue = new Color(0, 0, 150, 255);
	private Color red = new Color(150, 0, 0, 255);
	private JList<GameCharacter> turnList;
	private DefaultListModel<GameCharacter> turnModel;
	private JScrollPane panedList;

	public TurnPanel(DefaultListModel<GameCharacter> turnModel) {

		this.turnModel = turnModel;
		this.setSize(550, 75);
		this.setLayout(new GridLayout(2, 1));
		setBackground(MainDisplay.backgroundColor);
		setOpaque(true);
		this.add(new JLabel());
		turnList = new JList<GameCharacter>(turnModel);

		panedList = new JScrollPane(turnList);
		panedList
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panedList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		panedList.setAlignmentY(CENTER_ALIGNMENT);
		panedList.setBackground(MainDisplay.backgroundColor);
		panedList.setOpaque(true);
		
		turnList.setCellRenderer(new TurnListRenderer());
		turnList.setVisibleRowCount(1);
		turnList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		turnList.setBackground(MainDisplay.backgroundColor);
		turnList.setOpaque(true);

		this.add(panedList);
	}

	public void addListener(ListSelectionListener listener) {
		turnList.addListSelectionListener(listener);
	}

	public void updateJList(DefaultListModel<GameCharacter> t) {
		panedList.remove(turnList);
		turnModel = t;
		ListSelectionListener[] listen = turnList.getListSelectionListeners();
		turnList = new JList<GameCharacter>(turnModel);
		for (ListSelectionListener l : listen) {
			turnList.addListSelectionListener(l);
		}
		turnList.setCellRenderer(new TurnListRenderer());
		turnList.setVisibleRowCount(1);
		turnList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		panedList.setViewportView(turnList);
		turnList.updateUI();
	}

	private class TurnListRenderer extends JLabel implements
			ListCellRenderer<GameCharacter> {

		public TurnListRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}

		@Override
		public Component getListCellRendererComponent(
				JList<? extends GameCharacter> list, GameCharacter value,
				int index, boolean isSelected, boolean cellHasFocus) {

			GameCharacter current = turnModel.getElementAt(index);
			String name = current.getName();
			TitledBorder border = BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					name, TitledBorder.LEFT,
					TitledBorder.DEFAULT_JUSTIFICATION, MainDisplay.turnFont,
					Color.WHITE);

			this.setPreferredSize(new Dimension(105, 75));

			if (current instanceof PlayableCharacter)
				this.setBackground(blue);

			else
				this.setBackground(red);

			this.setBorder(border);
			if (current instanceof MachineGunner)
				setIcon(new ImageIcon(new ImageIcon(
						MainDisplay.images.get(current.getImageFile()))
						.getImage().getScaledInstance(90, 45,
								Image.SCALE_SMOOTH)));
			else
				setIcon(new ImageIcon(new ImageIcon(
						MainDisplay.images.get(current.getImageFile()))
						.getImage().getScaledInstance(50, 50,
								Image.SCALE_SMOOTH)));
			return this;
		}
	}
}
