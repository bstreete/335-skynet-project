package display;

import items.GameItem;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ItemTableModel extends AbstractTableModel {

	private List<GameItem> items;

	public ItemTableModel() {

		this.items = new ArrayList<GameItem>();
	}

	public void clear() {
		items = new ArrayList<GameItem>();
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public int getRowCount() {
		return items.size();
	}

	@Override
	public GameItem getValueAt(int row, int col) {
		if (items.size() <= row || row < 0)
			return null;
		else
			return items.get(row);
	}

	public void updateItems(List<GameItem> newItems) {
		items = newItems;
	}
}
