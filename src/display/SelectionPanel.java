package display;

import items.GameItem;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import characters.GameCharacter;
import characters.GameObject;
import characters.playable.MachineGunner;

public class SelectionPanel extends JPanel {

	GameObject selection;

	public SelectionPanel(GameObject selected) {
		selection = selected;

		this.setPreferredSize(new Dimension(200, 300));

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		String description;
		// Black Background
		g2.setColor(MainDisplay.backgroundColor);
		g2.fillRect(0, 0, getWidth(), getHeight());

		// Draw Object's basic image

		// If a machineGunner
		if (selection instanceof MachineGunner)
			g2.drawImage(MainDisplay.images.get(selection.getImageFile()), 10,
					10, 120, 60, null);

		// Not a machinegunner
		else
			g2.drawImage(MainDisplay.images.get(selection.getImageFile()), 70,
					10, 60, 60, null);

		g2.setColor(Color.orange);
		g2.setFont(MainDisplay.selectedFont);

		// If a character, list stats, name, and abilities
		if (selection instanceof GameCharacter) {

			GameCharacter selected = (GameCharacter) selection;
			description = selected.getAbilityDescription();
			g2.drawString(selected.getName(), 140, 30);
			g2.drawString("Health: " + selected.getHealth(), 140, 50);
			g2.drawString("Range: " + selected.getRange(), 10, 90);
			g2.drawString("Mobility: " + selected.getMobility(), 10, 130);

			if (description.length() > 28) {
				int line = 0;
				for (String s : segmentDescription(description)) {
					g2.drawString(s, 10, 170 + (line * 15));
					line++;
				}
			} else
				g2.drawString(description, 10, 170);
		}

		// If an item, list name and description
		else if (selection instanceof GameItem) {
			GameItem selected = (GameItem) selection;
			description = selected.getDescription();
			g2.drawString(selected.getName(), 140, 30);

			if (description.length() > 28) {
				int line = 0;
				for (String s : segmentDescription(description)) {
					g2.drawString(s, 10, 90 + (line * 15));
					line++;
				}

			} else
				g2.drawString(description, 10, 170);
		}
	}

	/**
	 * Takes a string longer than 28 characters and splits it into segments.
	 * Each segment will contain full words, words won't be cut to fit the 28
	 * character limit. Each segment will be added to a list that is returned
	 * for output.
	 */
	private List<String> segmentDescription(String original) {
		ArrayList<String> output = new ArrayList<String>();
		String sub;

		while (original.length() > 28) {

			if (original.charAt(27) == ' ') {
				sub = original.substring(0, 28);
				original = original.substring(28);

			} else {
				int index = 27;

				while (original.charAt(index) != ' ')
					index--;

				sub = original.substring(0, index + 1);
				original = original.substring(index + 1);
			}

			output.add(sub);
		}

		output.add(original);
		return output;
	}

	/**
	 * Updates the selected character. Then repaints with the new information.
	 */
	public void updateSelected(GameObject newSelection) {
		if (newSelection != null) {
			selection = newSelection;
			this.repaint();
		}
	}
}
