package display;

import java.util.TimerTask;

import javax.swing.JPanel;

public class ImageTimer extends TimerTask {

	private JPanel source;

	/**
	 * Calls repaint on the JPanel passed in the constructor whenever run is
	 * called. Used to call repaint on a schedule to update images.
	 */
	public ImageTimer(JPanel source) {
		this.source = source;
	}

	@Override
	public void run() {
		source.repaint();
	}

}