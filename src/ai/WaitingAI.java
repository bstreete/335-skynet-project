package ai;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import maps.Tile;
import model.PathFinder;
import characters.GameCharacter;
import characters.playable.PlayableCharacter;

public class WaitingAI {
	//this AI won't pursue his enemies unless he is the last one alive
	public static Decision getDecision(GameCharacter selected,List<GameCharacter> good,List<GameCharacter> bad, Tile[][] map){
		if(selected==null){
			return new Decision(null,-1,-1,false);//skip turn if nobody selected
		}
		//if selected character can attack a character than attack
		for(GameCharacter goodChar: good){
				if(selected.canAttackAt(goodChar.getRow(), goodChar.getCol(), map)){
					return new Decision(selected, goodChar.getRow(), goodChar.getCol(),true);
				}
		}
		
		//if the enemy team is greater than the hero team, do not pursue enemies. If hp < 4000, will attack regardless.
		if(bad.size()>good.size()&&selected.getHealth()==4000){
			return new Decision(null,-1,-1,false);
		}
		//else move towards closest opponent using Dijkstra's algorithm
		Point source = new Point(selected.getRow(),selected.getCol());
		Point destination = new Point(-1,-1);
		Point[][] prev = new Point[map.length][map[0].length];
		int[][] distance = new int[map.length][map[0].length];
		ArrayList<Point> Q = new ArrayList<Point>();
		PathFinder.initializeHelpers(prev, distance, Q);
		distance[source.x][source.y]=0;
		while(!Q.isEmpty()){
			Point current = PathFinder.removeSmallest(Q,distance);
			if(distance[current.x][current.y]==9999999){
				break;
			}
			for(Point v: PathFinder.getNeighbors(current)){
				if(v.x<map.length&&v.x>=0&&v.y<map[0].length&&v.y>=0){
						int alt = distance[current.x][current.y]+1;
						//check if opponent is on tile
							if(map[v.x][v.y].getOccupant()!=null&& map[v.x][v.y].getOccupant() instanceof PlayableCharacter){
								distance[v.x][v.y]=alt;
								prev[v.x][v.y]=current;
								destination=new Point(v.x,v.y);
								break;
							}
							else if(map[v.x][v.y].isAccessible()){ //else check if it is accessible
								if(alt< distance[v.x][v.y]){
									distance[v.x][v.y]=alt;
									prev[v.x][v.y]=current;
								}
							}
							else{} //if not accessible, then ignore
				}
			}
			if(destination.x!=-1){
				break; //end loop if we found destination
			}
		}
		Point cur=destination;
		if(cur.x==-1){
			return new Decision(null,-1,-1,false); //if selected can't move, return skip decision
		}
		ArrayList<Point> path = new ArrayList<Point>(); //make a list of all points along path
		while(cur.x!=source.x||cur.y!=source.y){
			path.add(new Point(cur.x,cur.y));
			cur= prev[cur.x][cur.y];
		}
		Collections.reverse(path);//reverse the list
		if(path.size()<=1){
			return new Decision(null,-1,-1,false); //if selected can't move, return skip decision
		}
		if(selected.getMobility()>path.size()){ //if selected character has mobility larger than path
			Point decide = path.get(path.size()-2);//then move as close as you can to target
			return new Decision(selected,decide.x,decide.y,false);
		}
		else{
			Point decide = path.get(selected.getMobility()-1);//else move as far along path as you can
		return new Decision(selected,decide.x,decide.y,false);
		}
	}
}
