package ai;

import java.awt.Point;

import characters.GameCharacter;

public class Decision {
	// an AI's decision can be described by the unit he chooses to use, and the
	// coordinates he acts on
	private static final boolean MOVE = false;
	private static final boolean ATTACK = true;
	private GameCharacter selected;
	private int row;
	private int col;
	private boolean attack;

	public Decision(GameCharacter gc, int r, int c, boolean attackOrNot) {
		selected = gc;
		row = r;
		col = c;
		attack = attackOrNot;
	}

	public GameCharacter getSelectedCharacter() {
		return selected;
	}

	public Point getTarget() {
		return new Point(row, col);
	}
	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public boolean willAttack() {
		return attack;
	}
}